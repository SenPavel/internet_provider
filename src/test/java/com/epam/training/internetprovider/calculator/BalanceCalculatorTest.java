package com.epam.training.internetprovider.calculator;

import com.epam.training.internetprovider.entity.Payment;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class BalanceCalculatorTest {


    private static final BigDecimal EXPECTED = new BigDecimal(110);
    private static final Payment FIRST_PAYMENT = new Payment(1L, 1L, new BigDecimal(30), new BigDecimal(20), new Date(1));
    private static final Payment SECOND_PAYMENT = new Payment(2L, 1L, new BigDecimal(30), new BigDecimal(30), new Date(1));

    private List<Payment> payments = new ArrayList<>();

    {
        payments.add(FIRST_PAYMENT);
        payments.add(SECOND_PAYMENT);
    }


    @Test
    public void shouldCorrectCalculate() {
        MoneyCalculator<Payment> calculator = new BalanceCalculator();

        BigDecimal actual = calculator.calculate(payments);

        Assert.assertEquals(EXPECTED, actual);
    }
}
