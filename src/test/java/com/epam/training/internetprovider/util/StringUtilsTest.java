package com.epam.training.internetprovider.util;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {

    private static final String FIRST_STRING = "1000";
    private static final String SECOND_STRING = "Hello";
    private static final String NULL_STRING = null;
    private static final String EMPTY_STRING = " ";
    private static final String INTEGER_REGEX = "^[1-9][0-9]*$";

    @Test
    public void shouldCorrectCheckValuesIsNullOrEmpty() {
        Assert.assertFalse(StringUtils.isNullOrEmpty(FIRST_STRING));
        Assert.assertFalse(StringUtils.isNullOrEmpty(SECOND_STRING));
        Assert.assertTrue(StringUtils.isNullOrEmpty(NULL_STRING));
        Assert.assertTrue(StringUtils.isNullOrEmpty(EMPTY_STRING));
    }

    @Test
    public void shouldCorrectCheckRegexMatch() {
        Assert.assertTrue(StringUtils.regexMatch(INTEGER_REGEX, FIRST_STRING));
        Assert.assertFalse(StringUtils.regexMatch(INTEGER_REGEX, SECOND_STRING));
        Assert.assertFalse(StringUtils.regexMatch(INTEGER_REGEX, NULL_STRING));
        Assert.assertFalse(StringUtils.regexMatch(INTEGER_REGEX, EMPTY_STRING));
    }

    @Test
    public void shouldCorrectCheckStringLength() {
        Assert.assertTrue(StringUtils.lengthMoreThan(3, FIRST_STRING));
        Assert.assertTrue(StringUtils.lengthMoreThan(3, SECOND_STRING));
        Assert.assertTrue(StringUtils.lengthMoreThan(0, EMPTY_STRING));
        Assert.assertFalse(StringUtils.lengthMoreThan(2, NULL_STRING));
        Assert.assertFalse(StringUtils.lengthMoreThan(5, FIRST_STRING));
        Assert.assertFalse(StringUtils.lengthMoreThan(6, SECOND_STRING));
        Assert.assertFalse(StringUtils.lengthMoreThan(2, EMPTY_STRING));
    }
}
