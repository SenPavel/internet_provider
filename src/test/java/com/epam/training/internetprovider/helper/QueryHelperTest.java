package com.epam.training.internetprovider.helper;


import com.epam.training.internetprovider.entity.Tariff;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public class QueryHelperTest {

    private static final String EXPECTED_INSERT_SQL = "INSERT INTO tariff (id_tariff, name, price, download_speed, " +
            "unload_speed) VALUES ('1', 'Maximum', '10', '200', '300')";
    private static final String EXPECTED_UPDATE_SQL = "UPDATE tariff SET name = 'Maximum', price = '10', " +
            "download_speed = '200', unload_speed = '300' WHERE id_tariff = 1";
    private static final String EXPECTED_DELETE_SQL  = "DELETE FROM tariff WHERE id_tariff = 1";
    private Map<String, Object> fields = new LinkedHashMap<>();


    {
        fields.put(Tariff.ID_TARIFF, 1L);
        fields.put(Tariff.NAME, "Maximum");
        fields.put(Tariff.PRICE, new BigDecimal(10));
        fields.put(Tariff.DOWNLOAD_SPEED, 200L);
        fields.put(Tariff.UNLOAD_SPEED, 300L);
    }

    @Test
    public void shouldMakeInsertQuery() {
        //when
        String resultQuery = QueryHelper.makeInsertQuery(fields,  Tariff.TABLE_NAME);

        //then
        Assert.assertEquals(EXPECTED_INSERT_SQL, resultQuery);
    }

    @Test
    public void shouldMakeUpdateQuery() {
        //when
        String resultQuery = QueryHelper.makeUpdateQuery(fields, Tariff.TABLE_NAME);

        //then
        Assert.assertEquals(EXPECTED_UPDATE_SQL, resultQuery);
    }

    @Test
    public void shouldMakeDeleteQuery() {
        //when
        String resultQuery = QueryHelper.makeDeleteQuery(fields, Tariff.TABLE_NAME);

        //then
        Assert.assertEquals(EXPECTED_DELETE_SQL, resultQuery);
    }
}
