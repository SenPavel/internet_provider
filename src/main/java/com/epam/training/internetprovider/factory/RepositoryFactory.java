package com.epam.training.internetprovider.factory;

import com.epam.training.internetprovider.connection.ConnectionPool;
import com.epam.training.internetprovider.repository.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.Connection;

/**
 * Simple factory for creating entity repository.
 * Class implements {@link AutoCloseable} for using try-with-resources
 * for automatic return connection
 */
public class RepositoryFactory implements AutoCloseable {


    private ConnectionPool pool;
    private Connection connection;

    public RepositoryFactory() {
        pool = ConnectionPool.getInstance();
        connection = pool.getConnection();
    }

    public UserRepository createUserRepository() {
        return new UserRepository(connection);
    }

    public PaymentRepository createPaymentRepository() {
        return new PaymentRepository(connection);
    }

    public TariffRepository createTariffRepository() {
        return new TariffRepository(connection);
    }

    public TrafficRepository createTrafficRepository() {
        return new TrafficRepository(connection);
    }

    public DiscountRepository createDiscountRepository() {
        return new DiscountRepository(connection);
    }

    public void close() {
        pool.returnConnection(connection);
    }
}
