package com.epam.training.internetprovider.factory;

import com.epam.training.internetprovider.command.*;
import com.epam.training.internetprovider.constant.CommandEnum;

public class CommandFactory {

    public static Command create(String command) {
        return CommandEnum.getCommandByCommandName(command);
    }
}
