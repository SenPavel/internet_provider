package com.epam.training.internetprovider.constant;

import com.epam.training.internetprovider.command.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration for storing commands and their actions
 */
public enum CommandEnum {

    ADD_PAYMENT("addPayment", new AddPaymentCommand()),
    ADD_USER("addUser", new AddUserCommand()),
    UPDATE_DISCOUNTS("updateDiscounts", new UpdateDiscountsCommand()),
    BLOCK_USER("blockUser", new BlockAndUnblockCommand()),
    CHANGE_USER_TARIFF("changeUserTariff", new ChangeUserTariffCommand()),
    DELETE_TARIFF("deleteTariff", new DeleteTariffCommand()),
    DELETE_DISCOUNT("deleteDiscount", new DeleteDiscountCommand()),
    LOGIN("login", new LoginCommand()),
    LOGOUT("logout", new LogOutCommand()),
    SHOW_INFORMATION("showInformation", new ShowInformationCommand()),
    SHOW_PAYMENTS("showPayments", new ShowPaymentCommand()),
    SHOW_TARIFFS("showTariffs", new ShowTariffsCommand(PageConstant.TARIFFS)),
    SHOW_TARIFFS_ADMIN("showTariffsAdmin", new ShowTariffsCommand(PageConstant.TARIFFS_ADMIN)),
    SHOW_TRAFFIC("showTraffic", new ShowTrafficCommand()),
    SHOW_MAIN("showMain", new ShowPageCommand(PageConstant.MAIN)),
    SHOW_SETTINGS("showSettings", new ShowPageCommand(PageConstant.SETTINGS)),
    SHOW_USERS("showUsers", new ShowUsersCommand()),
    SHOW_DISCOUNTS("showDiscounts", new ShowDiscountsCommand()),
    UPDATE_PERSONAL_INFORMATION("updatePersonalInformation", new UpdateUserInformationCommand()),
    UPDATE_SECURITY_INFORMATION("updateSecurityInformation", new UpdateUserSecurityInformationCommand()),
    UPDATE_TARIFFS("updateTariffs", new UpdateTariffsCommand());

    /**
     * Url for commands mapping
     */
    private static final String COMMAND_URL = "/controller?command=";

    private static Map<String, Command> commandMap = initializeMap();

    private final String commandName;
    private final Command command;

    CommandEnum(String commandName, Command command) {
        this.commandName = commandName;
        this.command = command;
    }

    /**
     * @param commandName
     * @return {@link Command} if it exists, otherwise return null
     */
    public static Command getCommandByCommandName(String commandName) {
        if (commandMap.containsKey(commandName)) {
            return commandMap.get(commandName);
        }
        return null;
    }

    /**
     * Add commands name and command actions to map
     * @return
     */
    private static Map<String, Command> initializeMap() {
        Map<String, Command> commandMap = new HashMap<>();
        for (CommandEnum commandEnum : CommandEnum.values()) {
            commandMap.put(commandEnum.commandName, commandEnum.command);
        }
        return commandMap;
    }

    public String getCommandName() {
        return commandName;
    }

    public Command getCommand() {
        return command;
    }

    /**
     * @return full Url for command, for example: /controller?command=login
     */
    public String getCommandUrl() {
        return COMMAND_URL + commandName;
    }
}
