package com.epam.training.internetprovider.constant;

/**
 * Constants for parameters names
 */
public final class ParamNameConstant {

    public static final String BALANCE = "balance";
    public static final String COMMAND = "command";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String DISCOUNTS = "discounts";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String OLD_PASSWORD = "old_password";
    public static final String PAGE = "page";
    public static final String PAGES_COUNT = "pagesCount";
    public static final String PAYMENTS = "payments";
    public static final String TARIFF = "tariff";
    public static final String TARIFFS = "tariffs";
    public static final String TRAFFICS = "traffics";
    public static final String SUCCESSFUL = "successful";
    public static final String USER = "user";
    public static final String USERS = "users";

    /**
     * Constants for  {@link com.epam.training.internetprovider.entity.Discount}
     */
    public static final String ID_DISCOUNT = "id_discount";
    public static final String NAME = "name";
    public static final String DISCOUNT = "discount";
    public static final String EXPIRATION_DATE = "expiration_date";

    /**
     * Constants for {@link com.epam.training.internetprovider.entity.Payment}
     */
    public static final String ID_PAYMENT = "id_payment";
    public static final String PAY_OUT = "pay_out";
    public static final String PAY_IN = "pay_in";
    public static final String DATE = "date";

    /**
     * Constants for {@link com.epam.training.internetprovider.entity.Tariff}
     */
    public static final String ID_TARIFF = "id_tariff";
    public static final String PRICE = "price";
    public static final String DISCOUNT_PRICE = "discount_price";
    public static final String DOWNLOAD_SPEED = "download_speed";
    public static final String UNLOAD_SPEED = "unload_speed";

    /**
     * Constants for {@link com.epam.training.internetprovider.entity.User}
     */
    public static final String ID_USER = "id_user";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String SURNAME = "surname";
    public static final String IS_ADMIN = "is_admin";
    public static final String IS_BLOCKED = "is_blocked";

    private ParamNameConstant() {
    }
}
