package com.epam.training.internetprovider.constant;

public final class PageConstant {

    public static final String ERROR_PAGE = "/WEB-INF/pages/error.jsp";
    public static final String LOGIN = "/WEB-INF/pages/login.jsp";
    public static final String DISCOUNTS = "/WEB-INF/pages/discounts.jsp";
    public static final String INFORMATION = "/WEB-INF/pages/information.jsp";
    public static final String MAIN = "/WEB-INF/pages/main.jsp";
    public static final String PAYMENTS = "/WEB-INF/pages/payments.jsp";
    public static final String SETTINGS = "/WEB-INF/pages/settings.jsp";
    public static final String TARIFFS = "/WEB-INF/pages/tariffs.jsp";
    public static final String TARIFFS_ADMIN = "/WEB-INF/pages/tariffs-admin.jsp";
    public static final String TRAFFIC = "/WEB-INF/pages/traffic.jsp";
    public static final String USERS = "/WEB-INF/pages/users.jsp";

    private PageConstant() {}
}
