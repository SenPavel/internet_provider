package com.epam.training.internetprovider.controller;

import com.epam.training.internetprovider.command.Command;
import com.epam.training.internetprovider.command.CommandResult;
import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.factory.CommandFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Controller for /controller url path  
 */
public class Controller extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    /**
     * Method get command parameter, create action and do redirect and dispatch
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter(ParamNameConstant.COMMAND);
        LOGGER.info("Command from request: " + command);
        Command action = CommandFactory.create(command);
        CommandResult result;
        try {
            result = action.execute(request, response);
            if (result.isRedirect()) {
                redirect(request, response, result.getPage());
            } else {
                dispatch(request, response, result.getPage());
            }
        } catch (ServiceException ex) {
            LOGGER.error(ex);
            request.setAttribute(ParamNameConstant.ERROR_MESSAGE, ex.getMessage());
            dispatch(request, response, PageConstant.ERROR_PAGE);
        }
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    private void redirect(HttpServletRequest request, HttpServletResponse response, String page) throws IOException {
        response.sendRedirect(page);
    }
}
