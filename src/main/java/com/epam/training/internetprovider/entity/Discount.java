package com.epam.training.internetprovider.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Class represent a discount
 */
public class Discount implements Serializable {

    public static final String TABLE_NAME = "discount";
    public static final String ID_DISCOUNT = "id_discount";
    public static final String NAME = "name";
    public static final String DISCOUNT = "discount";
    public static final String EXPIRATION_DATE = "expiration_date";

    private static final long serialVersionUID = 1L;

    private final Long id;
    private final String name;
    private final Integer discount;
    private final Timestamp expirationDate;

    public Discount(Long id, String name, Integer discount, Timestamp expirationDate) {
        this.id = id;
        this.name = name;
        this.discount = discount;
        this.expirationDate = expirationDate;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getDiscount() {
        return discount;
    }

    public Timestamp getExpirationDate() {
        return expirationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Discount discount1 = (Discount) o;
        return Objects.equals(id, discount1.id) &&
                Objects.equals(name, discount1.name) &&
                Objects.equals(discount, discount1.discount) &&
                Objects.equals(expirationDate, discount1.expirationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, discount, expirationDate);
    }
}
