package com.epam.training.internetprovider.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Class represent a tariff
 */

public class Tariff implements Serializable {

    public static final String ID_TARIFF = "id_tariff";
    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String DISCOUNT_PRICE = "discount_price";
    public static final String DOWNLOAD_SPEED = "download_speed";
    public static final String UNLOAD_SPEED = "unload_speed";
    public static final String TABLE_NAME = "tariff";

    private static final long serialVersionUID = 3L;

    private final Long id;
    private final Long idDiscount;
    private final String name;
    private final BigDecimal price;
    private final BigDecimal discountPrice;
    private final Long downloadSpeed;
    private final Long unloadSpeed;

    public Tariff(Long id, Long idDiscount, String name, BigDecimal price, BigDecimal discountPrice,
                  Long downloadSpeed, Long unloadSpeed) {
        this.id = id;
        this.idDiscount = idDiscount;
        this.name = name;
        this.price = price;
        this.discountPrice = discountPrice;
        this.downloadSpeed = downloadSpeed;
        this.unloadSpeed = unloadSpeed;
    }

    public Long getId() {
        return id;
    }

    public Long getIdDiscount() {
        return idDiscount;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public Long getDownloadSpeed() {
        return downloadSpeed;
    }

    public Long getUnloadSpeed() {
        return unloadSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tariff tariff = (Tariff) o;
        return Objects.equals(id, tariff.id) &&
                Objects.equals(idDiscount, tariff.idDiscount) &&
                Objects.equals(name, tariff.name) &&
                Objects.equals(price, tariff.price) &&
                Objects.equals(discountPrice, tariff.discountPrice) &&
                Objects.equals(downloadSpeed, tariff.downloadSpeed) &&
                Objects.equals(unloadSpeed, tariff.unloadSpeed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idDiscount, name, price, discountPrice, downloadSpeed, unloadSpeed);
    }
}
