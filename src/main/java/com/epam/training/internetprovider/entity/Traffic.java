package com.epam.training.internetprovider.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Class represent a traffic
 */

public class Traffic implements Serializable {

    public static final String ID_TRAFFIC = "id_traffic";
    public static final String OUTCOMING = "outcoming";
    public static final String INCOMING = "incoming";
    public static final String TABLE_NAME = "traffic";
    public static final String START_SESSION = "start_session";
    public static final String END_SESSION = "end_session";

    private static final long serialVersionUID = 1L;

    private final Long id;
    private final Long idUser;
    private final Timestamp startSession;
    private final Timestamp endSession;
    private final Long outcoming;
    private final Long incoming;


    public Traffic(Long id, Long idUser, Timestamp startSession, Timestamp endSession, Long outcoming, Long incoming) {
        this.id = id;
        this.idUser = idUser;
        this.startSession = startSession;
        this.endSession = endSession;
        this.outcoming = outcoming;
        this.incoming = incoming;
    }

    public Long getId() {
        return id;
    }

    public Long getIdUser() {
        return idUser;
    }

    public Long getOutcoming() {
        return outcoming;
    }

    public Long getIncoming() {
        return incoming;
    }

    public Timestamp getStartSession() {
        return startSession;
    }

    public Timestamp getEndSession() {
        return endSession;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Traffic traffic = (Traffic) o;
        return Objects.equals(id, traffic.id) &&
                Objects.equals(idUser, traffic.idUser) &&
                Objects.equals(startSession, traffic.startSession) &&
                Objects.equals(endSession, traffic.endSession) &&
                Objects.equals(outcoming, traffic.outcoming) &&
                Objects.equals(incoming, traffic.incoming);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idUser, startSession, endSession, outcoming, incoming);
    }
}
