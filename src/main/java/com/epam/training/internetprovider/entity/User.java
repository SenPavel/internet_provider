package com.epam.training.internetprovider.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class represent a user
 */

public class User implements Serializable {

    public static final String ID_USER = "id_user";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String SURNAME = "surname";
    public static final String IS_ADMIN = "is_admin";
    public static final String IS_BLOCKED = "is_blocked";
    public static final String TABLE_NAME = "user";

    private static final long serialVersionUID = 1L;

    private final Long id;
    private final Long idTariff;
    private final String login;
    private final String password;
    private final String firstName;
    private final String lastName;
    private final String surname;
    private final Boolean admin;
    private final Boolean blocked;

    public User(Long id, Long idTariff, String login, String password, String firstName, String lastName,
                String surname, Boolean admin, Boolean blocked) {
        this.id = id;
        this.idTariff = idTariff;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.surname = surname;
        this.admin = admin;
        this.blocked = blocked;
    }

    public Long getId() {
        return id;
    }

    public Long getIdTariff() {
        return idTariff;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSurname() {
        return surname;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return admin == user.admin &&
                blocked == user.blocked &&
                Objects.equals(id, user.id) &&
                Objects.equals(idTariff, user.idTariff) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(surname, user.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idTariff, login, password, firstName, lastName, surname, admin, blocked);
    }
}
