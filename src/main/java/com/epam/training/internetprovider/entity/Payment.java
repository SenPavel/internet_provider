package com.epam.training.internetprovider.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

/**
 * Class represent a payment
 */

public class Payment implements Serializable {

    public static final String ID_PAYMENT = "id_payment";
    public static final String PAY_OUT = "pay_out";
    public static final String PAY_IN = "pay_in";
    public static final String DATE = "date";
    public static final String TABLE_NAME = "payment";

    private static final long serialVersionUID = 1L;

    private final Long idPayment;
    private final Long idUser;
    private final BigDecimal payOut;
    private final BigDecimal payIn;
    private final Date date;

    public Payment(Long idPayment, Long idUser, BigDecimal payOut, BigDecimal payIn, Date date) {
        this.idPayment = idPayment;
        this.idUser = idUser;
        this.payOut = payOut;
        this.payIn = payIn;
        this.date = date;
    }

    public Long getIdPayment() {
        return idPayment;
    }

    public Long getIdUser() {
        return idUser;
    }

    public BigDecimal getPayOut() {
        return payOut;
    }

    public BigDecimal getPayIn() {
        return payIn;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return Objects.equals(idPayment, payment.idPayment) &&
                Objects.equals(idUser, payment.idUser) &&
                Objects.equals(payOut, payment.payOut) &&
                Objects.equals(payIn, payment.payIn) &&
                Objects.equals(date, payment.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPayment, idUser, payOut, payIn, date);
    }
}
