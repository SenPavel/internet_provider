package com.epam.training.internetprovider.service.traffic;

import com.epam.training.internetprovider.entity.Traffic;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.factory.RepositoryFactory;
import com.epam.training.internetprovider.repository.TrafficRepository;
import com.epam.training.internetprovider.specification.FindByIdUserSpecification;
import com.epam.training.internetprovider.specification.FindTrafficByIdUserWithLimitSpecification;
import com.epam.training.internetprovider.specification.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class TrafficServiceImpl implements TrafficService {

    private static final Logger LOGGER = LogManager.getLogger(TrafficServiceImpl.class);

    @Override
    public List<Traffic> findAllWithLimitByUserId(Long id, Long limit, Long rowFromNumber) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            TrafficRepository repository = factory.createTrafficRepository();
            Specification specification = new FindTrafficByIdUserWithLimitSpecification(id, limit, rowFromNumber);
            return repository.queryGetAll(specification);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public Long getFieldsCount() throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            TrafficRepository repository = creator.createTrafficRepository();
            return repository.getFieldsCount();
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }
}
