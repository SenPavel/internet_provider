package com.epam.training.internetprovider.service.discount;

import com.epam.training.internetprovider.factory.RepositoryFactory;
import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.repository.DiscountRepository;
import com.epam.training.internetprovider.specification.FindAllSpecification;
import com.epam.training.internetprovider.specification.Specification;

import java.util.List;

public class DiscountServiceImpl implements DiscountService {
    @Override
    public List<Discount> findAll() throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            DiscountRepository repository = creator.createDiscountRepository();
            Specification specification = new FindAllSpecification();
            return repository.queryGetAll(specification);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public void delete(Discount discount) throws ServiceException {

        try (RepositoryFactory creator = new RepositoryFactory()) {
            DiscountRepository repository = creator.createDiscountRepository();
            repository.remove(discount);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public void save(Discount discount) throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            DiscountRepository repository = creator.createDiscountRepository();
            repository.save(discount);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }
}
