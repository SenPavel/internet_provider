package com.epam.training.internetprovider.service.payment;

import com.epam.training.internetprovider.calculator.BalanceCalculator;
import com.epam.training.internetprovider.calculator.MoneyCalculator;
import com.epam.training.internetprovider.entity.Payment;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.factory.RepositoryFactory;
import com.epam.training.internetprovider.repository.PaymentRepository;
import com.epam.training.internetprovider.specification.FindPaymentsByIdUserWithLimitSpecification;
import com.epam.training.internetprovider.specification.FindByIdUserSpecification;
import com.epam.training.internetprovider.specification.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class PaymentServiceImpl implements PaymentService {

    @Override
    public void save(Payment payment) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            PaymentRepository repository = factory.createPaymentRepository();
            repository.save(payment);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public List<Payment> findAllWithLimitByUserId(Long id, Long rowCount, Long rowFrom) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            PaymentRepository repository = factory.createPaymentRepository();
            Specification specification = new FindPaymentsByIdUserWithLimitSpecification(id, rowCount, rowFrom);
            return repository.queryGetAll(specification);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public BigDecimal currentBalanceByUserId(Long id) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            PaymentRepository repository = factory.createPaymentRepository();
            Specification specification = new FindByIdUserSpecification(id);
            List<Payment> payments = repository.queryGetAll(specification);
            MoneyCalculator<Payment> calculator = new BalanceCalculator();
            return calculator.calculate(payments);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public Long getFieldsCount() throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            PaymentRepository repository = creator.createPaymentRepository();
            return repository.getFieldsCount();
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }
}
