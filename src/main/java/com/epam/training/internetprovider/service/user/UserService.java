package com.epam.training.internetprovider.service.user;

import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;

import java.util.List;
import java.util.Optional;

/**
 * The <tt>UserService</tt> interface encapsulates business logic for User
 *
 * @see User
 */

public interface UserService {

    /**
     * Method find user object from database for login
     * @param login
     * @param password
     * @return user object
     * @throws ServiceException
     */
    Optional<User> login(String login, String password)throws ServiceException;

    /**
     * @param id
     * @return user object
     * @throws ServiceException
     */
    Optional<User> findById(Long id) throws ServiceException;

    /**
     * Method save user object in database
     * @param user
     * @throws ServiceException
     */
    void save(User user) throws ServiceException;

    /**
     * @return list of all users
     * @throws ServiceException
     */
    List<User> findAll() throws ServiceException;

    /**
     * @param login
     * @return user object
     * @throws ServiceException
     */
    Optional<User> findByLogin(String login) throws ServiceException;

}
