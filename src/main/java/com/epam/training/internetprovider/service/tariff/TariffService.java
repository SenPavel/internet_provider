package com.epam.training.internetprovider.service.tariff;

import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.exception.ServiceException;

import java.util.List;
import java.util.Optional;

/**
 * The <tt>TariffService</tt> interface encapsulates business logic for Tariff
 *
 * @see Tariff
 */

public interface TariffService {

    /**
     * @param id user id
     * @return user tariff
     * @throws ServiceException
     */
    Optional<Tariff> findById(Long id) throws ServiceException;

    /**
     * @return list of all tariffs
     * @throws ServiceException
     */
    List<Tariff> findAll() throws ServiceException;

    /**
     * Method save tariff object in database
     * @param tariff
     * @throws ServiceException
     */
    void save(Tariff tariff) throws ServiceException;

    /**
     * Method delete tariff object from database
     * @param tariff
     * @throws ServiceException
     */
    void delete(Tariff tariff) throws ServiceException;
}
