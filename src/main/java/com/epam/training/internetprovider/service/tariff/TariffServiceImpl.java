package com.epam.training.internetprovider.service.tariff;

import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.factory.RepositoryFactory;
import com.epam.training.internetprovider.repository.TariffRepository;
import com.epam.training.internetprovider.specification.FindAllSpecification;
import com.epam.training.internetprovider.specification.FindByIdTariffSpecification;
import com.epam.training.internetprovider.specification.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

public class TariffServiceImpl implements TariffService {


    @Override
    public Optional<Tariff> findById(Long id) throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            TariffRepository repository = creator.createTariffRepository();
            Specification specification = new FindByIdTariffSpecification(id);
            return repository.queryForSingleResult(specification);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public List<Tariff> findAll() throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            TariffRepository repository = creator.createTariffRepository();
            Specification specification = new FindAllSpecification();
            return repository.queryGetAll(specification);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public void save(Tariff tariff) throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            TariffRepository repository = creator.createTariffRepository();
            repository.save(tariff);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public void delete(Tariff tariff) throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            TariffRepository repository = creator.createTariffRepository();
            repository.remove(tariff);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }
}
