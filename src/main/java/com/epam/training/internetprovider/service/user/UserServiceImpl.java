package com.epam.training.internetprovider.service.user;

import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.factory.RepositoryFactory;
import com.epam.training.internetprovider.repository.UserRepository;
import com.epam.training.internetprovider.specification.*;
import com.lambdaworks.crypto.SCryptUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {

    @Override
    public Optional<User> login(String login, String password) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            UserRepository repository = factory.createUserRepository();
            Specification specification = new FindByUserLoginSpecification(login);
            Optional<User> user = repository.queryForSingleResult(specification);

            if (user.isPresent() && SCryptUtil.check(password, user.get().getPassword())) {
                return user;
            } else {
                return Optional.empty();
            }
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public void save(User user) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            UserRepository repository = factory.createUserRepository();
            repository.save(user);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public List<User> findAll() throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            UserRepository repository = creator.createUserRepository();
            Specification specification = new FindAllSpecification();
            return repository.queryGetAll(specification);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public Optional<User> findById(Long id) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            UserRepository repository = factory.createUserRepository();
            Specification specification = new FindByIdUserSpecification(id);
            return repository.queryForSingleResult(specification);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }

    @Override
    public Optional<User> findByLogin(String login) throws ServiceException {
        try (RepositoryFactory creator = new RepositoryFactory()) {
            UserRepository repository = creator.createUserRepository();
            Specification specification = new FindByUserLoginSpecification(login);
            return repository.queryForSingleResult(specification);
        } catch (RepositoryException ex) {
            throw new ServiceException(ex);
        }
    }
}
