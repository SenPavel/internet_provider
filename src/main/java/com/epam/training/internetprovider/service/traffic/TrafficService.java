package com.epam.training.internetprovider.service.traffic;

import com.epam.training.internetprovider.entity.Traffic;
import com.epam.training.internetprovider.exception.ServiceException;

import java.util.List;

/**
 * The <tt>TrafficService</tt> interface encapsulates business logic for Traffic
 *
 * @see Traffic
 */

public interface TrafficService {

    /**
     * Method find all traffic by user id with limit
     * @param id user id
     * @param rowCount row limit
     * @param rowFrom line number from which to start select
     * @return list of traffic
     * @throws ServiceException
     */
    List<Traffic> findAllWithLimitByUserId(Long id, Long rowCount, Long rowFrom) throws ServiceException;

    /**
     *
     * @return number of rows in the table
     * @throws ServiceException
     */
    Long getFieldsCount() throws ServiceException;
}
