package com.epam.training.internetprovider.service.payment;

import com.epam.training.internetprovider.entity.Payment;
import com.epam.training.internetprovider.exception.ServiceException;

import java.math.BigDecimal;
import java.util.List;

/**
 * The <tt>PaymentService</tt> interface encapsulates business logic for Payment
 *
 * @see Payment
 */

public interface PaymentService {

    /**
     * Method save payment object in database
     * @param payment
     * @throws ServiceException
     */
    void save(Payment payment) throws ServiceException;

    /**
     * Method find all discount by user id with limit
     * @param id user id
     * @param rowCount row limit
     * @param rowFrom line number from which to start select
     * @return list of payments
     * @throws ServiceException
     */
    List<Payment> findAllWithLimitByUserId(Long id, Long rowCount, Long rowFrom) throws ServiceException;

    /**
     *
     * @param id user id
     * @return current user balance
     * @throws ServiceException
     */
    BigDecimal currentBalanceByUserId(Long id) throws ServiceException;

    /**
     *
     * @return number of rows in the table
     * @throws ServiceException
     */
    Long getFieldsCount() throws ServiceException;
}
