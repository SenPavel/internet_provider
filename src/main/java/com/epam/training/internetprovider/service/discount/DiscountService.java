package com.epam.training.internetprovider.service.discount;

import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.exception.ServiceException;

import java.util.List;

/**
 * The <tt>DiscountService</tt> interface encapsulates business logic for Discount
 *
 * @see Discount
 */
public interface DiscountService {

    /**
     * @return List with all discounts
     * @throws ServiceException
     */
    List<Discount> findAll() throws ServiceException;

    /**
     * Method delete discount object from database
     * @param discount
     * @throws ServiceException
     */
    void delete(Discount discount) throws ServiceException;

    /**
     * Method save discount object in database
     * @param discount
     * @throws ServiceException
     */
    void save(Discount discount) throws ServiceException;
}
