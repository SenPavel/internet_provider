package com.epam.training.internetprovider.exception;

/**
 * Provides exception for {@link com.epam.training.internetprovider.connection.ConnectionPool}
 */

public class ConnectionPoolException extends RuntimeException {

    public ConnectionPoolException(Throwable cause) {
        super(cause);
    }

    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }
}
