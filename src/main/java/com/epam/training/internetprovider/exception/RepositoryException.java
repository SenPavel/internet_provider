package com.epam.training.internetprovider.exception;

/**
 * Provides exception for {@link com.epam.training.internetprovider.repository.Repository}
 */

public class RepositoryException extends Exception {
    public RepositoryException(Throwable cause) {
        super(cause);
    }

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
