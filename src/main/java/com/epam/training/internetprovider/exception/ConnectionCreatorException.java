package com.epam.training.internetprovider.exception;


/**
 * Provides exception for {@link com.epam.training.internetprovider.connection.ConnectionCreator}
 */
public class ConnectionCreatorException extends Exception {
    public ConnectionCreatorException(Throwable cause) {
        super(cause);
    }

    public ConnectionCreatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
