package com.epam.training.internetprovider.exception;

/**
 * Provides exception for service classes
 */

public class ServiceException extends Exception {
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
