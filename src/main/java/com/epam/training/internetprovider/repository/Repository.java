package com.epam.training.internetprovider.repository;

import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.specification.Specification;

import java.util.List;
import java.util.Optional;

/**
 *  The <tt>Repository</tt> interface provides two methods
 *  to save, remove object and three methods to get all objects from table,
 *  get one object from table and get table fields count
 *  whose parameters are in <tt>HttpServletRequest</tt>
 * @param <T>
 */
public interface Repository<T> {

    /**
     * Saves object in database
     * @param item
     * @throws RepositoryException
     */
    void save(T item) throws RepositoryException;

    /**
     * Removes object from database
     * @param item
     * @throws RepositoryException
     */
    void remove(T item) throws RepositoryException;

    /**
     * @param specification selection condition
     * @return List of objects for database
     * @throws RepositoryException
     */
    List<T> queryGetAll(Specification specification) throws RepositoryException;

    /**
     * @param specification selection condition
     * @return Object from database
     * @throws RepositoryException
     */
    Optional<T> queryForSingleResult(Specification specification) throws RepositoryException;

    /**
     * @return table fields count
     * @throws RepositoryException
     */
    Long getFieldsCount() throws RepositoryException;
}
