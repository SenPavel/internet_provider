package com.epam.training.internetprovider.repository;

import com.epam.training.internetprovider.builder.resultset.PaymentBuilderFromResultSet;
import com.epam.training.internetprovider.entity.Payment;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.specification.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PaymentRepository extends AbstractRepository<Payment> {

    private static final String SQL_SELECT_ALL = "SELECT * FROM payment WHERE ";

    public PaymentRepository(Connection connection) {
        super(connection);
    }


    @Override
    public List<Payment> queryGetAll(Specification specification) throws RepositoryException {
        return executeQuery(SQL_SELECT_ALL + specification.toSql(), new PaymentBuilderFromResultSet(),
                specification.getParameters());
    }

    @Override
    protected Map<String, Object> getFields(Payment item) {
        Map<String, Object> fields = new LinkedHashMap<>();

        fields.put(Payment.ID_PAYMENT, item.getIdPayment());
        fields.put(User.ID_USER, item.getIdUser());
        fields.put(Payment.PAY_IN, item.getPayIn());
        fields.put(Payment.PAY_OUT, item.getPayOut());
        fields.put(Payment.DATE, item.getDate());

        return fields;
    }

    @Override
    protected String getTableName() {
        return Payment.TABLE_NAME;
    }

    @Override
    protected boolean exists(Payment item) {
        return item.getIdPayment() != null;
    }
}
