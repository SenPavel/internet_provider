package com.epam.training.internetprovider.repository;

import com.epam.training.internetprovider.builder.resultset.BuilderFromResultSet;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.helper.QueryHelper;
import com.epam.training.internetprovider.specification.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;

/**
 * The <tt>AbstractRepository</tt> abstract class implement methods
 * from {@link Repository} interface and provides three methods
 * for get table fields, table name and for checking Whether
 * the user is in the database
 *
 * @param <T>
 *
 * @see DiscountRepository
 * @see PaymentRepository
 * @see TariffRepository
 * @see TrafficRepository
 * @see UserRepository
 */
public abstract class AbstractRepository<T> implements Repository<T>{

    private static final Logger LOGGER = LogManager.getLogger(AbstractRepository.class);
    private static final String SQL_FIELDS_COUNT = "SELECT COUNT(*) FROM ";

    private Connection connection;

    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }

    public void save(T item) throws RepositoryException {
        LOGGER.debug("Saving the object in database");
        String query;
        if (exists(item)) {
            query = QueryHelper.makeUpdateQuery(getFields(item), getTableName());
        } else {
            query = QueryHelper.makeInsertQuery(getFields(item), getTableName());
        }
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();
        } catch (SQLException ex) {
            throw new RepositoryException(ex);
        }
    }

    @Override
    public void remove(T item) throws RepositoryException {
        LOGGER.debug("Remove object form database");
        String query = QueryHelper.makeDeleteQuery(getFields(item), getTableName());
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new RepositoryException(ex);
        }
    }

    public Optional<T> queryForSingleResult(Specification specification) throws RepositoryException {
        LOGGER.debug("Get object from database");
        List<T> objects = queryGetAll(specification);
        if (objects.size() == 1) {
            return Optional.of(objects.get(0));
        } else {
            return Optional.empty();
        }
    }

    public Long getFieldsCount() throws RepositoryException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIELDS_COUNT + getTableName());
             ResultSet resultSet = preparedStatement.executeQuery()) {

            resultSet.next();
            return resultSet.getLong(1);
        } catch (SQLException ex) {
            throw new RepositoryException(ex);
        }
    }

    protected List<T> executeQuery(String query, BuilderFromResultSet<T> builderFromResultSet, List<Object> params) throws RepositoryException {
        LOGGER.debug("Executing query: " + query);
        List<T> entities = new ArrayList<>();

        try(PreparedStatement preparedStatement = connection.prepareStatement(query);) {
            for (int i = 0; i < params.size(); i++) {
                preparedStatement.setObject(i + 1, params.get(i));
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                entities.add(builderFromResultSet.build(resultSet));
            }
        } catch (SQLException ex) {
            throw new RepositoryException(ex);
        }
        return entities;
    }

    /**
     * @param item
     * @return Map with column name and values
     */
    protected abstract Map<String, Object> getFields(T item);

    /**
     *
     * @return table name
     */
    protected abstract String getTableName();

    /**
     *
     * @param item
     * @return true if object exists in database, otherwise false
     */
    protected abstract boolean exists(T item);
}
