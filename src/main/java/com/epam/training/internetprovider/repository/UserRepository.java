package com.epam.training.internetprovider.repository;

import com.epam.training.internetprovider.builder.resultset.UserBuilderFromResultSet;
import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.specification.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UserRepository extends AbstractRepository<User> {

    private static final String SQL_SELECT_ALL = "SELECT * FROM user WHERE ";

    public UserRepository(Connection connection) {
        super(connection);
    }

    @Override
    public List<User> queryGetAll(Specification specification) throws RepositoryException {
        return executeQuery(SQL_SELECT_ALL + specification.toSql(), new UserBuilderFromResultSet(),
                specification.getParameters());
    }

    @Override
    protected Map<String, Object> getFields(User item) {
        Map<String, Object> fields = new LinkedHashMap<>();

        fields.put(User.ID_USER, item.getId());
        fields.put(Tariff.ID_TARIFF, item.getIdTariff());
        fields.put(User.FIRST_NAME, item.getFirstName());
        fields.put(User.LAST_NAME, item.getLastName());
        if (item.getSurname() != null) {
            fields.put(User.SURNAME, item.getSurname());
        }
        fields.put(User.IS_ADMIN, item.getAdmin());
        fields.put(User.IS_BLOCKED, item.getBlocked());
        fields.put(User.LOGIN, item.getLogin());
        fields.put(User.PASSWORD, item.getPassword());

        return fields;
    }

    @Override
    protected String getTableName() {
        return User.TABLE_NAME;
    }

    @Override
    protected boolean exists(User item) {
        return item.getId() != null;
    }
}
