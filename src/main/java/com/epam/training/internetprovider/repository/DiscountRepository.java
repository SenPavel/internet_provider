package com.epam.training.internetprovider.repository;

import com.epam.training.internetprovider.builder.resultset.DiscountBuilderFromResultSet;
import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.specification.Specification;

import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DiscountRepository extends AbstractRepository<Discount> {

    private static final String SQL_SELECT_ALL = "SELECT * FROM discount WHERE ";

    public DiscountRepository(Connection connection) {
        super(connection);
    }

    @Override
    public List<Discount> queryGetAll(Specification specification) throws RepositoryException {
        return executeQuery(SQL_SELECT_ALL + specification.toSql(), new DiscountBuilderFromResultSet(),
                specification.getParameters());
    }

    @Override
    protected Map<String, Object> getFields(Discount item) {
        Map<String, Object> fields = new LinkedHashMap<>();
        fields.put(Discount.ID_DISCOUNT, item.getId());
        fields.put(Discount.NAME, item.getName());
        fields.put(Discount.DISCOUNT, item.getDiscount());
        fields.put(Discount.EXPIRATION_DATE, item.getExpirationDate());
        return fields;
    }

    @Override
    protected String getTableName() {
        return Discount.TABLE_NAME;
    }

    @Override
    protected boolean exists(Discount item) {
        return item.getId() != null;
    }


}
