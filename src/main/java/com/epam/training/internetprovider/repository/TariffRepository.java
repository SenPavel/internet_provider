package com.epam.training.internetprovider.repository;

import com.epam.training.internetprovider.builder.resultset.TariffBuilderFromResultSet;
import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.specification.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.util.*;

public class TariffRepository extends AbstractRepository<Tariff> {

    private static final String SQL_SELECT_ALL = "SELECT * FROM tariff WHERE ";

    public TariffRepository(Connection connection) {
        super(connection);
    }

    @Override
    public List<Tariff> queryGetAll(Specification specification) throws RepositoryException {
        return executeQuery(SQL_SELECT_ALL + specification.toSql(), new TariffBuilderFromResultSet(),
                specification.getParameters());
    }

    @Override
    protected Map<String, Object> getFields(Tariff item) {
        Map<String, Object> fields = new LinkedHashMap<>();
        fields.put(Tariff.ID_TARIFF, item.getId());
        fields.put(Discount.ID_DISCOUNT, item.getIdDiscount());
        fields.put(Tariff.NAME, item.getName());
        fields.put(Tariff.PRICE, item.getPrice());
        fields.put(Tariff.DISCOUNT_PRICE, item.getDiscountPrice());
        fields.put(Tariff.DOWNLOAD_SPEED, item.getDownloadSpeed());
        fields.put(Tariff.UNLOAD_SPEED, item.getUnloadSpeed());
        return fields;
    }

    @Override
    protected String getTableName() {
        return Tariff.TABLE_NAME;
    }

    @Override
    protected boolean exists(Tariff item) {
        return item.getId() != null;
    }
}
