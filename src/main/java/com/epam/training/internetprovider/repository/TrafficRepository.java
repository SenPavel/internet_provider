package com.epam.training.internetprovider.repository;

import com.epam.training.internetprovider.builder.resultset.TrafficBuilderFromResultSet;
import com.epam.training.internetprovider.entity.Traffic;
import com.epam.training.internetprovider.exception.RepositoryException;
import com.epam.training.internetprovider.specification.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TrafficRepository extends AbstractRepository<Traffic> {

    private static final String SQL_SELECT_ALL = "SELECT * FROM traffic WHERE ";

    public TrafficRepository(Connection connection) {
        super(connection);
    }

    @Override
    public List<Traffic> queryGetAll(Specification specification) throws RepositoryException {
        return executeQuery(SQL_SELECT_ALL + specification.toSql(), new TrafficBuilderFromResultSet(),
                specification.getParameters());
    }

    @Override
    protected Map<String, Object> getFields(Traffic item) {
        Map<String, Object> fields = new LinkedHashMap<>();
        fields.put("id_traffic", item.getId());
        fields.put("id_user", item.getIdUser());
        fields.put("outcoming", item.getOutcoming());
        fields.put("incoming", item.getIncoming());

        return fields;
    }

    @Override
    protected String getTableName() {
        return Traffic.TABLE_NAME;
    }

    @Override
    protected boolean exists(Traffic item) {
        return item.getId() != null;
    }
}
