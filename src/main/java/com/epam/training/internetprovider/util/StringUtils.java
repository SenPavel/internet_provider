package com.epam.training.internetprovider.util;

/**
 * Class provides methods to work with strings
 */
public class StringUtils {

    /**
     * The method checks parameters are empty or equal to null
     * @param values
     * @return true if one of the parameters is null or empty,
     * otherwise return false
     */
    public static boolean isNullOrEmpty(String... values) {
        for (String value : values) {
            if (value == null || value.trim().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * The method checks the parameters for matches with the regex
     * @param regex
     * @param values
     * @return true if all parameters match to regex,
     * otherwise return false
     */
    public static boolean regexMatch(String regex, String... values) {
        if (values.length == 1) {
            return values[0] != null && values[0].matches(regex);
        }
        for (String value : values) {
            if (isNullOrEmpty(value)) {
                continue;
            }
            if (!value.matches(regex)) {
                return false;
            }
        }
        return true;
    }

    /**
     * The method checks the number of characters for each of the parameters.
     * @param length
     * @param values
     * @return true if the number of characters less or equal length parameter,
     * otherwise return false
     */
    public static boolean lengthMoreThan(int length, String... values) {
        if (values.length == 1) {
            return values[0] != null && values[0].length() > length;
        }
        for (String value : values) {
            if (isNullOrEmpty(value)) {
                continue;
            }
            if (value.length() > length) {
                return true;
            }
        }
        return false;
    }
}
