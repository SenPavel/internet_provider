package com.epam.training.internetprovider.builder.resultset;

import com.epam.training.internetprovider.entity.Traffic;
import com.epam.training.internetprovider.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Implementation of the <tt>BuilderFromResultSet</tt> interface
 *  for building objects of type Traffic
 *  @see Traffic
 */

public class TrafficBuilderFromResultSet implements BuilderFromResultSet<Traffic> {

    private static final Logger LOGGER = LogManager.getLogger(TrafficBuilderFromResultSet.class);

    /**
     *
     * @param result ResultSet containing parameters to build an object
     * @return built Traffic object
     * @throws SQLException if something happened with database
     */
    @Override
    public Traffic build(ResultSet result) throws SQLException {
        LOGGER.debug("Building traffic from result set");

        Long id = result.getLong(Traffic.ID_TRAFFIC);
        Long idUser = result.getLong(User.ID_USER);
        Long outcoming = result.getLong(Traffic.OUTCOMING);
        Long incoming = result.getLong(Traffic.INCOMING);
        Timestamp startSession = result.getTimestamp(Traffic.START_SESSION);
        Timestamp endSession = result.getTimestamp(Traffic.END_SESSION);

        LOGGER.debug("Traffic was successful build");
        return new Traffic(id, idUser, startSession, endSession, outcoming, incoming);
    }
}
