package com.epam.training.internetprovider.builder.request;

import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.util.StringUtils;
import com.lambdaworks.crypto.SCryptUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Implementation of the <tt>BuilderFromRequest</tt> interface
 * for building objects of type User
 * @see User
 */
public class UserBuilderFromRequest implements BuilderFromRequest<User> {

    private static final Logger LOGGER = LogManager.getLogger(UserBuilderFromRequest.class);

    /**
     * In the method, all parameters passed to the <tt>HttpServletRequest</tt> are checked by
     * the utilization class method {@link com.epam.training.internetprovider.util.StringUtils#isNullOrEmpty(String...)},
     * if the parameter is empty or null, then it is assigned the value null
     * @param request HttpServletRequest containing parameters to build an object
     * @return built User object
     */

    @Override
    public User build(HttpServletRequest request) {
        LOGGER.debug("Building user from request");

        String idString = request.getParameter(ParamNameConstant.ID_USER);
        Long id = StringUtils.isNullOrEmpty(idString) ? null : Long.valueOf(idString);
        String idTariffString = request.getParameter(ParamNameConstant.ID_TARIFF);
        Long idTariff = StringUtils.isNullOrEmpty(idTariffString) ? null : Long.valueOf(idTariffString);

        String name = request.getParameter(ParamNameConstant.FIRST_NAME);
        name = StringUtils.isNullOrEmpty(name) ? null : name;
        String lastName = request.getParameter(ParamNameConstant.LAST_NAME);
        lastName = StringUtils.isNullOrEmpty(lastName) ? null : lastName;
        String surname = request.getParameter(ParamNameConstant.SURNAME);
        surname = StringUtils.isNullOrEmpty(surname) ? null : surname;
        String login = request.getParameter(ParamNameConstant.LOGIN);
        login = StringUtils.isNullOrEmpty(login) ? null : login;
        String password = request.getParameter(ParamNameConstant.PASSWORD);
        password = StringUtils.isNullOrEmpty(password) ? null : SCryptUtil.scrypt(password, 16, 16, 16);

        String isAdminString = request.getParameter(ParamNameConstant.IS_ADMIN);
        Boolean isAdmin = StringUtils.isNullOrEmpty(isAdminString) ? null : Boolean.valueOf(isAdminString);
        String isBlockedString = request.getParameter(ParamNameConstant.IS_BLOCKED);
        Boolean isBlocked = StringUtils.isNullOrEmpty(isBlockedString) ? null : Boolean.valueOf(isBlockedString);

        LOGGER.debug("User was successful build");
        return new User(id, idTariff, login, password, name, lastName, surname, isAdmin, isBlocked);
    }
}
