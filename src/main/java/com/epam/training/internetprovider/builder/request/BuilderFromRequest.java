package com.epam.training.internetprovider.builder.request;

import javax.servlet.http.HttpServletRequest;

/**
 *  The <tt>BuilderFromRequest</tt> provides one method
 *  to build object whose parameters are in <tt>HttpServletRequest</tt>
 *
 * @param <T> the type of object which will be build
 *
 * @see HttpServletRequest
 * @see DiscountBuilderFromRequest
 * @see PaymentBuilderFromRequest
 * @see TariffBuilderFromRequest
 * @see UserBuilderFromRequest
 */

public interface BuilderFromRequest<T> {

    /**
     * @param request HttpServletRequest containing parameters to build an object
     * @return the built object
     */
    T build(HttpServletRequest request);
}
