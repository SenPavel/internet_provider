package com.epam.training.internetprovider.builder.resultset;

import com.epam.training.internetprovider.entity.Discount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Implementation of the <tt>BuilderFromResultSet</tt> interface
 *  for building objects of type Discount
 *  @see Discount
 */

public class DiscountBuilderFromResultSet implements BuilderFromResultSet<Discount> {

    private static final Logger LOGGER = LogManager.getLogger(DiscountBuilderFromResultSet.class);
    /**
     * @param result ResultSet containing parameters to build an object
     * @return built Discount object
     * @throws SQLException if something happened with database
     */
    @Override
    public Discount build(ResultSet result) throws SQLException {
        LOGGER.debug("Building discount from result set");

        Long id = result.getLong(Discount.ID_DISCOUNT);
        String name = result.getString(Discount.NAME);
        Integer discount = result.getInt(Discount.DISCOUNT);
        Timestamp expirationDate = result.getTimestamp(Discount.EXPIRATION_DATE);

        LOGGER.debug("Discount was successful build");
        return new Discount(id, name, discount, expirationDate);
    }
}
