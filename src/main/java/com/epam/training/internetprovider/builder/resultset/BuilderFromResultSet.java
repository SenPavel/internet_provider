package com.epam.training.internetprovider.builder.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  The <tt>BuilderFromResultSet</tt> interface provides one method
 *  to build object whose values are in <tt>ResultSet</tt>
 *
 * @param <T> the type of object which will be build
 *
 * @see ResultSet
 * @see DiscountBuilderFromResultSet
 * @see PaymentBuilderFromResultSet
 * @see TariffBuilderFromResultSet
 * @see UserBuilderFromResultSet
 */

public interface BuilderFromResultSet<T> {

    /**
     * In the methods, the values that the <tt>ResultSet</tt> contains are assigned to variables.
     * @param result ResultSet containing parameters to build an object
     * @return the built object
     * @throws SQLException if something happened with database
     */
    T build(ResultSet result) throws SQLException;

}
