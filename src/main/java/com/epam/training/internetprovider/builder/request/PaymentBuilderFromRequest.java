package com.epam.training.internetprovider.builder.request;

import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Payment;
import com.epam.training.internetprovider.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

/**
 * Implementation of the <tt>BuilderFromRequest</tt> interface
 * for building objects of type Payment
 * @see Payment
 */
public class PaymentBuilderFromRequest implements BuilderFromRequest<Payment> {

    private static final Logger LOGGER = LogManager.getLogger(PaymentBuilderFromRequest.class);

    /**
     * @param request HttpServletRequest containing parameters to build an object
     * @return built Payment object
     */
    @Override
    public Payment build(HttpServletRequest request) {
        LOGGER.debug("Building payment from request");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ParamNameConstant.USER);

        Long idUser = user.getId();
        BigDecimal debit = BigDecimal.ZERO;
        BigDecimal credit = new BigDecimal(request.getParameter(Payment.PAY_IN));

        LOGGER.debug("Payment was successful build");
        return new Payment(null, idUser, debit, credit, null);
    }
}
