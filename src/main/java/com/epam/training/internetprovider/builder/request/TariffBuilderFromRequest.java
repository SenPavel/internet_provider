package com.epam.training.internetprovider.builder.request;

import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * Implementation of the <tt>BuilderFromRequest</tt> interface
 * for building objects of type Tariff
 * @see Tariff
 */
public class TariffBuilderFromRequest implements BuilderFromRequest<Tariff> {

    private static final Logger LOGGER = LogManager.getLogger(TariffBuilderFromRequest.class);
    /**
     * In the method, all parameters passed to the <tt>HttpServletRequest</tt> are checked by
     * the utilization class method {@link com.epam.training.internetprovider.util.StringUtils#isNullOrEmpty(String...)},
     * if the parameter is empty or null, then it is assigned the value null
     * @param request HttpServletRequest containing parameters to build an object
     * @return built Tariff object
     */
    @Override
    public Tariff build(HttpServletRequest request) {
        LOGGER.debug("Building tariff from request");

        String name = request.getParameter(ParamNameConstant.NAME);

        String idString = request.getParameter(ParamNameConstant.ID_TARIFF);
        Long id = StringUtils.isNullOrEmpty(idString) ? null : Long.valueOf(idString);
        String idDiscountString = request.getParameter(ParamNameConstant.ID_DISCOUNT);
        Long idDiscount = StringUtils.isNullOrEmpty(idDiscountString) ? null : Long.valueOf(idDiscountString);
        String priceString = request.getParameter(ParamNameConstant.PRICE);
        BigDecimal price = StringUtils.isNullOrEmpty(priceString) ? null : new BigDecimal(priceString);
        String discountPriceString = request.getParameter(ParamNameConstant.DISCOUNT_PRICE);
        BigDecimal discountPrice = StringUtils.isNullOrEmpty(discountPriceString) ? null : new BigDecimal(discountPriceString);
        String downloadSpeedString = request.getParameter(ParamNameConstant.DOWNLOAD_SPEED);
        Long downloadSpeed = StringUtils.isNullOrEmpty(downloadSpeedString) ? null : Long.valueOf(downloadSpeedString);
        String unloadSpeedString = request.getParameter(ParamNameConstant.UNLOAD_SPEED);
        Long unloadSpeed = StringUtils.isNullOrEmpty(unloadSpeedString) ? null : Long.valueOf(unloadSpeedString);

        LOGGER.debug("Tariff was successful build");
        return new Tariff(id, idDiscount, name, price, discountPrice, downloadSpeed, unloadSpeed);
    }
}
