package com.epam.training.internetprovider.builder.resultset;

import com.epam.training.internetprovider.entity.Payment;
import com.epam.training.internetprovider.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

/**
 * Implementation of the <tt>BuilderFromResultSet</tt> interface
 *  for building objects of type Payment
 *  @see Payment
 */

public class PaymentBuilderFromResultSet implements BuilderFromResultSet<Payment> {

    private static final Logger LOGGER = LogManager.getLogger(PaymentBuilderFromResultSet.class);

    /**
     * @param result ResultSet containing parameters to build an object
     * @return built Payment object
     * @throws SQLException if something happened with database
     */
    @Override
    public Payment build(ResultSet result) throws SQLException {
        LOGGER.debug("Building payment from result set");

        Long id = result.getLong(Payment.ID_PAYMENT);
        Long idUser = result.getLong(User.ID_USER);
        BigDecimal debit = result.getBigDecimal(Payment.PAY_OUT);
        BigDecimal credit = result.getBigDecimal(Payment.PAY_IN);
        Date date = result.getDate(Payment.DATE);

        LOGGER.debug("Payment was successful build");
        return new Payment(id, idUser, debit, credit, date);
    }

}
