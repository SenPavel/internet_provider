package com.epam.training.internetprovider.builder.request;

import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

/**
 * Implementation of the <tt>BuilderFromRequest</tt> interface
 * for building objects of type Discount
 * @see Discount
 */
public class DiscountBuilderFromRequest implements BuilderFromRequest<Discount> {

    private static final Logger LOGGER = LogManager.getLogger(DiscountBuilderFromRequest.class);

    /**
     * In the method, all parameters passed to the <tt>HttpServletRequest</tt> are checked by
     * the utilization class method {@link com.epam.training.internetprovider.util.StringUtils#isNullOrEmpty(String...)},
     * if the parameter is empty or null, then it is assigned the value null
     * @param request HttpServletRequest containing parameters to build an object
     * @return built Discount object
     */
    @Override
    public Discount build(HttpServletRequest request) {
        LOGGER.debug("Start building discount from request");

        String name = request.getParameter(ParamNameConstant.NAME);
        name = StringUtils.isNullOrEmpty(name) ? null : name;
        String idString = request.getParameter(ParamNameConstant.ID_DISCOUNT);
        Long id = StringUtils.isNullOrEmpty(idString) ? null : Long.valueOf(idString);
        String discountString = request.getParameter(ParamNameConstant.DISCOUNT);
        Integer discount = StringUtils.isNullOrEmpty(discountString) ? null : Integer.valueOf(discountString);
        String expirationDateString = request.getParameter(ParamNameConstant.EXPIRATION_DATE);
        Timestamp expirationDate = StringUtils.isNullOrEmpty(expirationDateString) ? null : Timestamp.valueOf(expirationDateString);

        return new Discount(id, name, discount, expirationDate);
    }
}
