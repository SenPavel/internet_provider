package com.epam.training.internetprovider.builder.resultset;

import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.entity.Tariff;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Implementation of the <tt>BuilderFromResultSet</tt> interface
 *  for building objects of type Tariff
 *  @see Tariff
 */

public class TariffBuilderFromResultSet implements BuilderFromResultSet<Tariff> {

    private static final Logger LOGGER = LogManager.getLogger(TariffBuilderFromResultSet.class);

    /**
     *
     * @param result ResultSet containing parameters to build an object
     * @return built Tariff object
     * @throws SQLException if something happened with database
     */

    @Override
    public Tariff build(ResultSet result) throws SQLException {
        LOGGER.debug("Building tariff from result set");

        Long id = result.getLong(Tariff.ID_TARIFF);
        Long idDiscount = result.getLong(Discount.ID_DISCOUNT);
        String name = result.getString(Tariff.NAME);
        BigDecimal price = result.getBigDecimal(Tariff.PRICE);
        BigDecimal discountPrice = result.getBigDecimal(Tariff.DISCOUNT_PRICE);
        Long downloadSpeed = result.getLong(Tariff.DOWNLOAD_SPEED);
        Long unloadSpeed = result.getLong(Tariff.UNLOAD_SPEED);

        LOGGER.debug("Tariff was successful build");
        return new Tariff(id, idDiscount, name, price, discountPrice, downloadSpeed, unloadSpeed);
    }
}
