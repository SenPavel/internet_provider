package com.epam.training.internetprovider.builder.resultset;

import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.entity.User;
import com.lambdaworks.crypto.SCryptUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Implementation of the <tt>BuilderFromResultSet</tt> interface
 *  for building objects of type User
 *  @see User
 */

public class UserBuilderFromResultSet implements BuilderFromResultSet<User> {

    private static final Logger LOGGER = LogManager.getLogger(UserBuilderFromResultSet.class);

    /**
     *
     * @param result ResultSet containing parameters to build an object
     * @return built User object
     * @throws SQLException if something happened with database
     */
    @Override
    public User build(ResultSet result) throws SQLException {
        LOGGER.debug("Building user from result set");

        Long id = result.getLong(User.ID_USER);
        Long idTariff = result.getLong(Tariff.ID_TARIFF);
        String name = result.getString(User.FIRST_NAME);
        String surname = result.getString(User.SURNAME);
        String lastName = result.getString(User.LAST_NAME);
        String login = result.getString(User.LOGIN);
        String password = result.getString(User.PASSWORD);
        Boolean isAdmin = result.getBoolean(User.IS_ADMIN);
        Boolean isBlocked = result.getBoolean(User.IS_BLOCKED);

        LOGGER.debug("User was successful build");
        return new User(id, idTariff, login, password, name, lastName, surname, isAdmin,
                isBlocked);
    }
}
