package com.epam.training.internetprovider.tag;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class PaginationTag extends TagSupport {

    private static final Logger LOGGER = LogManager.getLogger(PaginationTag.class);

    private String link;
    private Integer currentPage;
    private Integer pagesCount;

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.print("<ul id='paginate'>");
            if (currentPage <= 1) {
                out.print("<li><<</li>");
            } else {
                out.print("<a href=" + link + "&page=" + (currentPage - 1) + "><li><<</li></a>");
            }
            for (int i = 0; i < pagesCount; i++) {
                int page = i + 1;
                out.print("<a href=" + link + "&page=" + page + "><li>" + page + "</li></a>");
            }
            if (currentPage > pagesCount) {
                out.print("<li>>></li>");
            } else {
                out.print("<a href=" + link + "&page=" + (currentPage + 1) + "><li>>></li></a>");
            }
            out.print("</ul>");
        } catch (IOException ex) {
            LOGGER.error(ex);
        }
        return SKIP_BODY;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }
}
