package com.epam.training.internetprovider.helper;

import com.epam.training.internetprovider.builder.request.BuilderFromRequest;
import com.epam.training.internetprovider.builder.request.UserBuilderFromRequest;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.user.UserService;
import com.epam.training.internetprovider.service.user.UserServiceImpl;
import com.epam.training.internetprovider.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public class CommandHelper {

    private static final String INTEGER_REGEX = "^[1-9][0-9]*$";

    /**
     * Method update user information in database
     * and set updated user in session
     * @param request with user parameters
     * @throws ServiceException
     */
    public static void updateUserInfo(HttpServletRequest request) throws ServiceException {
        BuilderFromRequest<User> builder = new UserBuilderFromRequest();
        User user = builder.build(request);
        UserService service = new UserServiceImpl();
        service.save(user);
        Optional<User> optionalUser = service.findById(user.getId());
        HttpSession session = request.getSession();
        optionalUser.ifPresent(u -> session.setAttribute(ParamNameConstant.USER, u));
    }

    /**
     * Method calculate values and set attributes in request for pagination.
     * @param request
     * @param fieldsCountOnPage
     * @param fieldsCount
     * @return calculated Line number in the database where to start selecting rows
     */
    public static Long calculateAndSetAttrForPagination(HttpServletRequest request, Long fieldsCountOnPage, Long fieldsCount) {
        String currentPageString = request.getParameter(ParamNameConstant.PAGE);
        Long currentPage;
        if (StringUtils.isNullOrEmpty(currentPageString) || !currentPageString.matches(INTEGER_REGEX)) {
            currentPage = 1L;
        } else {
            currentPage = Long.valueOf(currentPageString);
        }
        int pagesCount = (int) Math.ceil(fieldsCount.doubleValue() / fieldsCountOnPage.doubleValue());
        request.setAttribute(ParamNameConstant.PAGES_COUNT, pagesCount);
        request.setAttribute(ParamNameConstant.CURRENT_PAGE, currentPage);
        return fieldsCountOnPage * (currentPage - 1);
    }
}
