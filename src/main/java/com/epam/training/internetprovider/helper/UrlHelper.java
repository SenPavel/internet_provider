package com.epam.training.internetprovider.helper;

import javax.servlet.http.HttpServletRequest;

public class UrlHelper {

    /**
     * Regex for query string, for example ?lang=en
     */
    private static final String REGEX = "([?&]|^[?&]|)(+)=(\\w+)";

    /**
     * Method delete from url query string
     * @param request
     * @param parameterKey
     * @return url without query string
     */
    public static String deleteParameterFromUrl(HttpServletRequest request, String parameterKey) {
        StringBuilder regexBuilder = new StringBuilder(REGEX);
        regexBuilder.insert(14, parameterKey);
        String url = request.getQueryString();
        url = url.replaceAll(regexBuilder.toString(), "");
        StringBuilder builder = new StringBuilder();
        builder.append(request.getRequestURL());
        if (request.getParameterMap().size() > 1) {
            builder.append("?");
        }
        builder.append(url);
        return builder.toString();
    }
}
