package com.epam.training.internetprovider.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class QueryHelper {

    /**
     * Method dynamically make insert sql query
     * @param fields contain columns names and values
     * @param table name of table
     * @return ready insert sql query
     */
    public static String makeInsertQuery(Map<String, Object> fields, String table) {
        StringBuilder builder = new StringBuilder("INSERT INTO ");
        builder.append(table).append(" (");
        for (String field : fields.keySet()) {
            if (fields.get(field) != null) {
                builder.append(field).append(", ");
            }
        }
        builder.delete(builder.length() - 2, builder.length());
        builder.append(") VALUES (");
        for (Object object : fields.values()) {
            if (object != null) {
                if (object instanceof Boolean) {
                    builder.append(object).append(", ");
                } else {
                    builder.append("'").append(object).append("', ");
                }
            }
        }

        builder.delete(builder.length() - 2, builder.length());
        builder.append(")");
        return builder.toString();
    }

    /**
     * Method dynamically make update sql query
     * @param fields contain columns names and values
     * @param table name of table
     * @return ready update sql query
     */
    public static String makeUpdateQuery(Map<String, Object> fields, String table) {
        StringBuilder builder = new StringBuilder("UPDATE ");
        builder.append(table).append(" SET ");
        for (String field : fields.keySet()) {
            if (field.equals("id_" + table)) {
                continue;
            }
            if (fields.get(field) != null) {
                builder.append(field).append(" = ").append(", ");
            }
        }

        int temp = 0;
        for (Object object : fields.values()) {
            if (temp == 0) {
                temp = builder.indexOf(" = ", temp) + 3;
                continue;
            }

            if (object != null) {
                if (object.equals(true) || object.equals(false)) {
                    builder.insert(temp, "" + object + "");
                } else {
                    builder.insert(temp, "'" + object + "'");
                }
                temp = builder.indexOf(" = ", temp) + 3;
            }
        }

        builder.delete(builder.length() - 2, builder.length());
        builder.append(" WHERE id_").append(table).append(" = ").append(fields.get("id_" + table));
        return builder.toString();
    }

    /**
     * Method dynamically make delete sql query
     * @param fields contain columns names and values
     * @param table name of table
     * @return ready delete sql query
     */
    public static String makeDeleteQuery(Map<String, Object> fields, String table) {
        StringBuilder builder = new StringBuilder("DELETE FROM ");
        builder.append(table).append(" WHERE ");
        builder.append("id_").append(table).append(" = ");
        builder.append(fields.get("id_" + table));

        return builder.toString();
    }
}
