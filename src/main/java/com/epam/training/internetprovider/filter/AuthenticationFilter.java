package com.epam.training.internetprovider.filter;

import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Filter for checking user authentication
 */

public class AuthenticationFilter implements Filter {

    private static final String LOGIN_PAGE = "/";
    private static final String EXTENSION_REGEX = ".*.css|.*.js|.*.jpg|.*.png";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * Method check user authentication, if user is not authenticated then
     * redirect to login page
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        String requestURI = request.getRequestURI();
        String command = request.getParameter(ParamNameConstant.COMMAND);
        if (requestURI.matches(EXTENSION_REGEX) || (command != null && command.equals(CommandEnum.LOGIN.getCommandName()))) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        if (session != null && session.getAttribute(ParamNameConstant.USER) != null || requestURI.equals(LOGIN_PAGE)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            response.sendRedirect(LOGIN_PAGE);
        }
    }

    @Override
    public void destroy() {

    }
}
