package com.epam.training.internetprovider.filter;

import com.epam.training.internetprovider.helper.UrlHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Filter for setting locale parameter in session
 */
public class LocaleFilter implements Filter {

    private static final String LOCALE_KEY_PARAMETER = "sessionLocale";
    private static final String LANG = "lang";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String localeParameter = request.getParameter(LOCALE_KEY_PARAMETER);

        if (localeParameter != null) {
            HttpSession session = request.getSession(false);
            session.setAttribute(LANG, localeParameter);
            String url = UrlHelper.deleteParameterFromUrl(request, LOCALE_KEY_PARAMETER);
            response.sendRedirect(url);
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
