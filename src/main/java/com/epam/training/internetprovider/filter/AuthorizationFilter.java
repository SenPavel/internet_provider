package com.epam.training.internetprovider.filter;

import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Filter for checking user authorization
 */

public class    AuthorizationFilter implements Filter {

    private Map<String, Boolean> adminCommand = new HashMap<>();
    private Map<String, Boolean> userCommand = new HashMap<>();

    /**
     * Puts in each map (admin and user) available command
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        adminCommand.put(CommandEnum.ADD_USER.getCommandName(), true);
        adminCommand.put(CommandEnum.BLOCK_USER.getCommandName(), true);
        adminCommand.put(CommandEnum.DELETE_TARIFF.getCommandName(), true);
        adminCommand.put(CommandEnum.DELETE_DISCOUNT.getCommandName(), true);
        adminCommand.put(CommandEnum.LOGIN.getCommandName(), true);
        adminCommand.put(CommandEnum.LOGOUT.getCommandName(), true);
        adminCommand.put(CommandEnum.SHOW_DISCOUNTS.getCommandName(), true);
        adminCommand.put(CommandEnum.SHOW_INFORMATION.getCommandName(), true);
        adminCommand.put(CommandEnum.SHOW_MAIN.getCommandName(), true);
        adminCommand.put(CommandEnum.SHOW_TARIFFS_ADMIN.getCommandName(), true);
        adminCommand.put(CommandEnum.SHOW_SETTINGS.getCommandName(), true);
        adminCommand.put(CommandEnum.SHOW_USERS.getCommandName(), true);
        adminCommand.put(CommandEnum.UPDATE_DISCOUNTS.getCommandName(), true);
        adminCommand.put(CommandEnum.UPDATE_PERSONAL_INFORMATION.getCommandName(), true);
        adminCommand.put(CommandEnum.UPDATE_SECURITY_INFORMATION.getCommandName(), true);
        adminCommand.put(CommandEnum.UPDATE_TARIFFS.getCommandName(), true);

        userCommand.put(CommandEnum.ADD_PAYMENT.getCommandName(), false);
        userCommand.put(CommandEnum.CHANGE_USER_TARIFF.getCommandName(), false);
        userCommand.put(CommandEnum.LOGIN.getCommandName(), false);
        userCommand.put(CommandEnum.LOGOUT.getCommandName(), false);
        userCommand.put(CommandEnum.SHOW_INFORMATION.getCommandName(), false);
        userCommand.put(CommandEnum.SHOW_MAIN.getCommandName(), false);
        userCommand.put(CommandEnum.SHOW_PAYMENTS.getCommandName(), false);
        userCommand.put(CommandEnum.SHOW_TARIFFS.getCommandName(), false);
        userCommand.put(CommandEnum.SHOW_TRAFFIC.getCommandName(), false);
        userCommand.put(CommandEnum.SHOW_SETTINGS.getCommandName(), false);
        userCommand.put(CommandEnum.UPDATE_PERSONAL_INFORMATION.getCommandName(), false);
        userCommand.put(CommandEnum.UPDATE_SECURITY_INFORMATION.getCommandName(), false);

    }

    /**
     * Method check user authorization, if the user tries to use an
     * unavailable command, the page returns with 404 errors
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();

        String command = request.getParameter(ParamNameConstant.COMMAND);
        if (command.equals(CommandEnum.LOGIN.getCommandName())) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        User user = (User) session.getAttribute(ParamNameConstant.USER);
        boolean isAdmin = user.getAdmin();
        Boolean adminPermission = adminCommand.get(command);
        Boolean userPermission = userCommand.get(command);
        if (adminPermission != null && adminPermission == isAdmin
                || userPermission != null && userPermission == isAdmin) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @Override
    public void destroy() {

    }
}
