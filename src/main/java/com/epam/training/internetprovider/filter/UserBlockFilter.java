package com.epam.training.internetprovider.filter;

import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Filter for checking is the user blocked
 */

public class UserBlockFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(UserBlockFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * If user blocked then set error message and forward to login page
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ParamNameConstant.USER);
        if (user != null && user.getBlocked()) {
            LOGGER.info("User with id: " + user.getId() + " blocked");
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.userBlocked");
            request.getRequestDispatcher(PageConstant.LOGIN).forward(request, response);
            session.invalidate();
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
