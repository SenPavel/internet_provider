package com.epam.training.internetprovider.calculator;

import java.math.BigDecimal;
import java.util.List;

/**
 *  The <tt>MoneyCalculator</tt> interface provides one method
 *  to calculate money which type is <tt>BigDecimal</tt> <tt>ResultSet</tt>
 *
 * @param <T> the type of object which contains money fields
 *
 * @see BalanceCalculator
 */

public interface MoneyCalculator<T> {

    /**
     *
     * @param items List of objects which contains money fields
     * @return calculated money
     */
    BigDecimal calculate(List<T> items);
}
