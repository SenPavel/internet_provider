package com.epam.training.internetprovider.calculator;

import com.epam.training.internetprovider.entity.Payment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.List;

/**
 * Implementation of the <tt>BalanceCalculator</tt> interface
 *  for calculating money in <tt>Payment</tt> objects
 *  @see Payment
 */

public class BalanceCalculator implements MoneyCalculator<Payment> {

    /**
     *
     * @param items List of objects which contains money fields
     * @return calculated money
     */
    @Override
    public BigDecimal calculate(List<Payment> items) {
        return calculateOutcoming(items).add(calculateIncoming(items));
    }

    /**
     * method calculate incoming fields in Payment objects
     * @param payments List of objects which contains incoming fields
     * @return calculated incoming
     */
    private BigDecimal calculateIncoming(List<Payment> payments) {
        BigDecimal result = BigDecimal.ZERO;
        for (Payment payment : payments) {
            result = result.add(payment.getPayIn());
        }

        return result;
    }

    /**
     * method calculate outcoming fields in Payment objects
     * @param payments List of objects which contains outcoming fields
     * @return calculated outcoming
     */
    private BigDecimal calculateOutcoming(List<Payment> payments) {
        BigDecimal result = BigDecimal.ZERO;
        for (Payment payment : payments) {
            result = result.add(payment.getPayOut());
        }

        return result;
    }
}
