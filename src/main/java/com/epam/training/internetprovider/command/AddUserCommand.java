package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.builder.request.BuilderFromRequest;
import com.epam.training.internetprovider.builder.request.UserBuilderFromRequest;
import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.user.UserService;
import com.epam.training.internetprovider.service.user.UserServiceImpl;
import com.epam.training.internetprovider.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Implementation of the <tt>Command</tt> interface for addUser command
 */

public class AddUserCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(AddUserCommand.class);
    private static final String LETTER_REGEX = "[\\p{L}\\s]+";

    /**
     * Method check parameters from HttpServletRequest and if they valid save user object in database
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return if user parameters in request was incorrect return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_USERS} and set value {@link ParamNameConstant#ERROR_MESSAGE}
     * in session for showing error message to user, otherwise return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_USERS} and set value {@link ParamNameConstant#SUCCESSFUL}
     * in session for showing success message to user
     * @throws ServiceException if something happened with UserServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String firstName = request.getParameter(ParamNameConstant.FIRST_NAME);
        String lastName = request.getParameter(ParamNameConstant.LAST_NAME);
        String surname = request.getParameter(ParamNameConstant.SURNAME);
        String idTariff = request.getParameter(ParamNameConstant.ID_TARIFF);
        String isAdmin = request.getParameter(ParamNameConstant.IS_ADMIN);
        String login = request.getParameter(ParamNameConstant.LOGIN);
        String password = request.getParameter(ParamNameConstant.PASSWORD);
        HttpSession session = request.getSession();

        if (StringUtils.isNullOrEmpty(firstName, lastName, idTariff, isAdmin, login, password)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.emptyFields");
            return CommandResult.redirect(CommandEnum.SHOW_USERS.getCommandUrl());
        }

        if (!StringUtils.regexMatch(LETTER_REGEX, firstName, lastName, surname)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.incorrectInformation");
        }

        if (StringUtils.lengthMoreThan(35, firstName, lastName, surname)
                || StringUtils.lengthMoreThan(20, login, password)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.fieldsTooBig");
            return CommandResult.redirect(CommandEnum.SHOW_USERS.getCommandUrl());
        }

        UserService service = new UserServiceImpl();
        Optional<User> optionalUser = service.findByLogin(login);
        if (optionalUser.isPresent()) {
            LOGGER.warn(login + "already taken");
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.takenLogin");
            return CommandResult.redirect(CommandEnum.SHOW_USERS.getCommandUrl());
        } else {
            BuilderFromRequest<User> builder = new UserBuilderFromRequest();
            User user = builder.build(request);
            service.save(user);
            session.setAttribute(ParamNameConstant.SUCCESSFUL, true);

            LOGGER.info("User with login = " + user.getLogin() + "was successfully add");
            return CommandResult.redirect(CommandEnum.SHOW_USERS.getCommandUrl());
        }
    }
}
