package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.discount.DiscountService;
import com.epam.training.internetprovider.service.discount.DiscountServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Implementation of the <tt>Command</tt> interface for showDiscounts command
 */

public class ShowDiscountsCommand implements Command {

    /**
     * Method findAll discounts and set their in request
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.redirect with parameter {@link PageConstant#DISCOUNTS}
     * @throws ServiceException if something happened with DiscountServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        DiscountService service = new DiscountServiceImpl();
        List<Discount> discounts = service.findAll();
        request.setAttribute(ParamNameConstant.DISCOUNTS, discounts);

        return CommandResult.forward(PageConstant.DISCOUNTS);
    }
}
