package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.tariff.TariffService;
import com.epam.training.internetprovider.service.tariff.TariffServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Implementation of the <tt>Command</tt> interface for showInformation command
 */

public class ShowInformationCommand implements Command {

    /**
     * Method find information about user in current session discounts and set its in request
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.forward with parameter {@link PageConstant#INFORMATION}
     * @throws ServiceException if something happened with UserServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ParamNameConstant.USER);
        TariffService service = new TariffServiceImpl();
        Optional<Tariff> optional = service.findById(user.getIdTariff());
        optional.ifPresent(tariff -> request.setAttribute(ParamNameConstant.TARIFF, tariff));

        return CommandResult.forward(PageConstant.INFORMATION);
    }
}
