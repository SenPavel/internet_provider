package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.helper.CommandHelper;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Implementation of the <tt>Command</tt> interface for changeUserTariff command
 */

public class ChangeUserTariffCommand implements Command {

    /**
     *
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.redirect with parameter {@link CommandEnum#SHOW_TARIFFS}
     * throws ServiceException if something happened with UserServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        CommandHelper.updateUserInfo(request);
        HttpSession session = request.getSession();
        session.setAttribute(ParamNameConstant.SUCCESSFUL, true);
        return CommandResult.redirect(CommandEnum.SHOW_TARIFFS.getCommandUrl());
    }
}
