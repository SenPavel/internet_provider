package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.discount.DiscountService;
import com.epam.training.internetprovider.service.discount.DiscountServiceImpl;
import com.epam.training.internetprovider.service.tariff.TariffService;
import com.epam.training.internetprovider.service.tariff.TariffServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Implementation of the <tt>Command</tt> interface for showTariffs command
 */

public class ShowTariffsCommand implements Command {

    private String tariffsPage;

    public ShowTariffsCommand(String tariffsPage) {
        this.tariffsPage = tariffsPage;
    }

    /**
     * Method find all tariffs and set their in request
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.forward with parameter {@link #tariffsPage}
     * @throws ServiceException if something happened with DiscountServiceImpl or TariffServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        TariffService tariffService = new TariffServiceImpl();
        List<Tariff> tariffs = tariffService.findAll();
        request.setAttribute(ParamNameConstant.TARIFFS, tariffs);
        DiscountService discountService = new DiscountServiceImpl();
        List<Discount> discounts = discountService.findAll();
        request.setAttribute(ParamNameConstant.DISCOUNTS, discounts);

        return CommandResult.forward(tariffsPage);
    }
}
