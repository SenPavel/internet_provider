package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.PageConstant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Implementation of the <tt>Command</tt> interface for logout command
 */

public class LogOutCommand implements Command {

    private static final String LOGIN_PAGE = "/";

    /**
     * Method invalidate session
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.redirect with parameter {@link PageConstant#LOGIN}
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession(false);
        session.invalidate();

        return CommandResult.redirect(LOGIN_PAGE);
    }
}
