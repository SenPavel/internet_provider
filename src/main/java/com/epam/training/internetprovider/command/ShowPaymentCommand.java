package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Payment;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.helper.CommandHelper;
import com.epam.training.internetprovider.service.payment.PaymentService;
import com.epam.training.internetprovider.service.payment.PaymentServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Implementation of the <tt>Command</tt> interface for showPayments command
 */

public class ShowPaymentCommand implements Command {

    private static final Long PAYMENTS_COUNT_IN_ONE_PAGE = 15L;

    /**
     * Method find payments with limit and set their in request, also set attributes for pagination
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.forward with parameter {@link PageConstant#PAYMENTS}
     * @throws ServiceException if something happened with PaymentServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        PaymentService service = new PaymentServiceImpl();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ParamNameConstant.USER);
        Long paymentFieldsCount = service.getFieldsCount();
        Long rowFromNumber = CommandHelper.calculateAndSetAttrForPagination(request, PAYMENTS_COUNT_IN_ONE_PAGE, paymentFieldsCount);
        List<Payment> payments = service.findAllWithLimitByUserId(user.getId(), PAYMENTS_COUNT_IN_ONE_PAGE, rowFromNumber);
        request.setAttribute(ParamNameConstant.PAYMENTS, payments);

        return CommandResult.forward(PageConstant.PAYMENTS);
    }
}
