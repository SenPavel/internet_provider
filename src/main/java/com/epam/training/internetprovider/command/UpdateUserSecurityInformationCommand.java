package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.helper.CommandHelper;
import com.epam.training.internetprovider.service.user.UserService;
import com.epam.training.internetprovider.service.user.UserServiceImpl;
import com.epam.training.internetprovider.util.StringUtils;
import com.lambdaworks.crypto.SCryptUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Implementation of the <tt>Command</tt> interface for updateUserSecurityInformation command
 */

public class UpdateUserSecurityInformationCommand implements Command {


    /**
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return if user parameters in request was incorrect return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_SETTINGS} and set value {@link ParamNameConstant#ERROR_MESSAGE}
     * in session for showing error message to user, otherwise return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_SETTINGS} and set value {@link ParamNameConstant#SUCCESSFUL}
     * in session for showing success message to user
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String login = request.getParameter(ParamNameConstant.LOGIN);
        String oldPassword = request.getParameter(ParamNameConstant.OLD_PASSWORD);
        String password = request.getParameter(ParamNameConstant.PASSWORD);
        HttpSession session = request.getSession();

        if (StringUtils.isNullOrEmpty(login) && StringUtils.isNullOrEmpty(password)
                && StringUtils.isNullOrEmpty(oldPassword)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.allFieldsEmpty");
            return CommandResult.redirect(CommandEnum.SHOW_SETTINGS.getCommandUrl());
        }

        if (StringUtils.lengthMoreThan(20, login, oldPassword, password)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.fieldsTooBig");
            return CommandResult.redirect(CommandEnum.SHOW_SETTINGS.getCommandUrl());
        }

        User sessionUser = (User) session.getAttribute(ParamNameConstant.USER);
        UserService service = new UserServiceImpl();
        if (service.findByLogin(login).isPresent()) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.takenLogin");
            return CommandResult.redirect(CommandEnum.SHOW_SETTINGS.getCommandUrl());
        }

        if (!StringUtils.isNullOrEmpty(oldPassword) && !SCryptUtil.check(oldPassword, sessionUser.getPassword())) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.wrongOldPassword");
            return CommandResult.redirect(CommandEnum.SHOW_SETTINGS.getCommandUrl());
        }

        CommandHelper.updateUserInfo(request);
        session.setAttribute(ParamNameConstant.SUCCESSFUL, true);
        return CommandResult.redirect(CommandEnum.SHOW_SETTINGS.getCommandUrl());
    }
}
