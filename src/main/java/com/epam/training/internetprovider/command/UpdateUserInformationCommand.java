package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.helper.CommandHelper;
import com.epam.training.internetprovider.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Implementation of the <tt>Command</tt> interface for updateUserInformation command
 */

public class UpdateUserInformationCommand implements Command {

    private static final String LETTER_REGEX = "[\\p{L}\\s]+";

    /**
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return if user parameters in request was incorrect return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_SETTINGS} and set value {@link ParamNameConstant#ERROR_MESSAGE}
     * in session for showing error message to user, otherwise return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_SETTINGS} and set value {@link ParamNameConstant#SUCCESSFUL}
     * in session for showing success message to user
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String firstName = request.getParameter(ParamNameConstant.FIRST_NAME);
        String lastName = request.getParameter(ParamNameConstant.LAST_NAME);
        String surname = request.getParameter(ParamNameConstant.SURNAME);
        HttpSession session = request.getSession();
        if (StringUtils.isNullOrEmpty(firstName) && StringUtils.isNullOrEmpty(lastName)
                && StringUtils.isNullOrEmpty(surname)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.allFieldsEmpty");
            return CommandResult.redirect(CommandEnum.SHOW_SETTINGS.getCommandUrl());
        }

        if (StringUtils.lengthMoreThan(15, firstName, lastName, surname)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.fieldsTooBig");
            return CommandResult.redirect(CommandEnum.SHOW_SETTINGS.getCommandUrl());
        }

        if (!StringUtils.regexMatch(LETTER_REGEX, firstName, lastName, surname)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.incorrectInformation");
            return CommandResult.redirect(CommandEnum.SHOW_SETTINGS.getCommandUrl());
        }

        CommandHelper.updateUserInfo(request);
        session.setAttribute(ParamNameConstant.SUCCESSFUL, true);
        return CommandResult.redirect(CommandEnum.SHOW_SETTINGS.getCommandUrl());
    }
}
