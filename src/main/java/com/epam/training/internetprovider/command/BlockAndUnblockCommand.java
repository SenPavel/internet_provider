package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.builder.request.BuilderFromRequest;
import com.epam.training.internetprovider.builder.request.UserBuilderFromRequest;
import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.user.UserService;
import com.epam.training.internetprovider.service.user.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Implementation of the <tt>Command</tt> interface for blockUser command
 */

public class BlockAndUnblockCommand implements Command {

    /**
     *
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.redirect with parameter {@link CommandEnum#SHOW_USERS}
     * throws ServiceException if something happened with UserServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        BuilderFromRequest<User> builder = new UserBuilderFromRequest();
        User user = builder.build(request);
        UserService service = new UserServiceImpl();
        service.save(user);

        return CommandResult.redirect(CommandEnum.SHOW_USERS.getCommandUrl());
    }
}
