package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.builder.request.BuilderFromRequest;
import com.epam.training.internetprovider.builder.request.DiscountBuilderFromRequest;
import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.discount.DiscountService;
import com.epam.training.internetprovider.service.discount.DiscountServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Implementation of the <tt>Command</tt> interface for deleteDiscount command
 */

public class DeleteDiscountCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(DeleteDiscountCommand.class);
    /**
     *
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.forward with parameter {@link CommandEnum#SHOW_DISCOUNTS}
     * throws ServiceException if something happened with DiscountServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        BuilderFromRequest<Discount> builder = new DiscountBuilderFromRequest();
        Discount discount = builder.build(request);
        DiscountService service = new DiscountServiceImpl();
        service.delete(discount);

        LOGGER.info("Discount with id: " + discount.getId() + "was successful delete");
        return CommandResult.redirect(CommandEnum.SHOW_DISCOUNTS.getCommandUrl());
    }
}
