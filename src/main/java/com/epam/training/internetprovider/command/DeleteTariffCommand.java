package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.builder.request.BuilderFromRequest;
import com.epam.training.internetprovider.builder.request.TariffBuilderFromRequest;
import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.tariff.TariffService;
import com.epam.training.internetprovider.service.tariff.TariffServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Implementation of the <tt>Command</tt> interface for deleteTariff command
 */

public class DeleteTariffCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(DeleteTariffCommand.class);

    /**
     *
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.forward with parameter {@link CommandEnum#SHOW_TARIFFS}
     * throws ServiceException if something happened with TariffServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        BuilderFromRequest<Tariff> builder = new TariffBuilderFromRequest();
        Tariff tariff = builder.build(request);
        TariffService service = new TariffServiceImpl();
        service.delete(tariff);

        LOGGER.info("Tariff with id: " + tariff.getId() + "was successful delete");
        return CommandResult.redirect(CommandEnum.SHOW_TARIFFS_ADMIN.getCommandUrl());
    }
}
