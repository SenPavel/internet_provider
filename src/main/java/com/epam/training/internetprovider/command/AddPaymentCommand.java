package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.builder.request.BuilderFromRequest;
import com.epam.training.internetprovider.builder.request.PaymentBuilderFromRequest;
import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Payment;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.payment.PaymentService;
import com.epam.training.internetprovider.service.payment.PaymentServiceImpl;
import com.epam.training.internetprovider.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

/**
 * Implementation of the <tt>Command</tt> interface for save command
 */

public class AddPaymentCommand implements Command {

    private static final String PAYMENT_REGEX = "^\\s*(?=.*[1-9])\\d*(?:\\.\\d{1,2})?\\s*$";
    private static final Logger LOGGER = LogManager.getLogger(AddPaymentCommand.class);

    /**
     * Method check parameters from HttpServletRequest and if they valid save payment object in database
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return if parameter payment in request was incorrect return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_PAYMENTS} and set value {@link ParamNameConstant#ERROR_MESSAGE}
     * in session for showing error message to user, otherwise return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_PAYMENTS} and set value {@link ParamNameConstant#SUCCESSFUL}
     * in session for showing success message to user
     * @throws ServiceException if something happened with PaymentServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String payIn = request.getParameter(ParamNameConstant.PAY_IN);
        HttpSession session = request.getSession();
        if (StringUtils.isNullOrEmpty(payIn) || !payIn.matches(PAYMENT_REGEX)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.incorrectPayment");
            LOGGER.warn("Incorrect payment value");
            return CommandResult.redirect(CommandEnum.SHOW_PAYMENTS.getCommandUrl());
        }

        BuilderFromRequest<Payment> builder = new PaymentBuilderFromRequest();
        Payment payment = builder.build(request);
        PaymentService service = new PaymentServiceImpl();
        service.save(payment);

        User user = (User) session.getAttribute(ParamNameConstant.USER);
        BigDecimal balance = service.currentBalanceByUserId(user.getId());
        session.setAttribute(ParamNameConstant.BALANCE, balance.doubleValue());
        session.setAttribute(ParamNameConstant.SUCCESSFUL, true);

        LOGGER.info("Payment was successful add");
        return CommandResult.redirect(CommandEnum.SHOW_PAYMENTS.getCommandUrl());
    }
}
