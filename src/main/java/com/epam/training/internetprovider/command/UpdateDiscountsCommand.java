package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.builder.request.BuilderFromRequest;
import com.epam.training.internetprovider.builder.request.DiscountBuilderFromRequest;
import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Discount;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.discount.DiscountService;
import com.epam.training.internetprovider.service.discount.DiscountServiceImpl;
import com.epam.training.internetprovider.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Implementation of the <tt>Command</tt> interface for updateDiscounts command
 */

public class UpdateDiscountsCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(UpdateDiscountsCommand.class);

    /**
     * Percent regex for values in range 1-100
     */
    private static final String PERCENT_REGEX = "^[1-9][0-9]?$|^100$";
    /**
     * TimeStamp regex for values like 2019:01:29 01:59:00
     */
    private static final String TIMESTAMP_REGEX = "[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]";

    /**
     *
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return if discount parameters in request was incorrect return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_DISCOUNTS} and set value {@link ParamNameConstant#ERROR_MESSAGE}
     * in session for showing error message to user, otherwise return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_DISCOUNTS} and set value {@link ParamNameConstant#SUCCESSFUL}
     * in session for showing success message to user
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String name = request.getParameter(ParamNameConstant.NAME);
        String discounts = request.getParameter(ParamNameConstant.DISCOUNT);
        String expirationDate = request.getParameter(ParamNameConstant.EXPIRATION_DATE);
        HttpSession session = request.getSession();
        if (StringUtils.isNullOrEmpty(name, discounts, expirationDate)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.emptyFields");
            return CommandResult.redirect(CommandEnum.SHOW_DISCOUNTS.getCommandUrl());
        }

        if (StringUtils.lengthMoreThan(15, name)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.fieldsTooBig");
            return CommandResult.redirect(CommandEnum.SHOW_DISCOUNTS.getCommandUrl());
        }

        if (!StringUtils.regexMatch(PERCENT_REGEX, discounts)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.incorrectPercent");
            return CommandResult.redirect(CommandEnum.SHOW_DISCOUNTS.getCommandUrl());
        }

        if (!StringUtils.regexMatch(TIMESTAMP_REGEX, expirationDate)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.incorrectDate");
            return CommandResult.redirect(CommandEnum.SHOW_DISCOUNTS.getCommandUrl());
        }

        BuilderFromRequest<Discount> builder = new DiscountBuilderFromRequest();
        Discount discount = builder.build(request);
        DiscountService service = new DiscountServiceImpl();
        service.save(discount);
        session.setAttribute(ParamNameConstant.SUCCESSFUL, true);

        LOGGER.info("Discount with id: " + discount.getId() + "was successful update");
        return CommandResult.redirect(CommandEnum.SHOW_DISCOUNTS.getCommandUrl());
    }
}
