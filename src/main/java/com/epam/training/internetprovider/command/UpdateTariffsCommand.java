package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.builder.request.BuilderFromRequest;
import com.epam.training.internetprovider.builder.request.TariffBuilderFromRequest;
import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.tariff.TariffService;
import com.epam.training.internetprovider.service.tariff.TariffServiceImpl;
import com.epam.training.internetprovider.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Implementation of the <tt>Command</tt> interface for updateTariffs command
 */

public class UpdateTariffsCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(UpdateTariffsCommand.class);
    /**
     * Price regex for values like 100, 1.20, 1.0 and etc
     */
    private static final String PRICE_REGEX = "^\\s*(?=.*[1-9])\\d*(?:\\.\\d{1,2})?\\s*$";
    private static final String INTEGER_REGEX = "^[1-9][0-9]*$";

    /**
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return if tariff parameters in request was incorrect returnCommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_TARIFFS_ADMIN} and set value {@link ParamNameConstant#ERROR_MESSAGE}
     * in session for showing error message to user, otherwise return CommandResult.redirect
     * with parameter {@link CommandEnum#SHOW_TARIFFS_ADMIN} and set value {@link ParamNameConstant#SUCCESSFUL}
     * in session for showing success message to user
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String name = request.getParameter(ParamNameConstant.NAME);
        String price = request.getParameter(ParamNameConstant.PRICE);
        String downloadSpeed = request.getParameter(ParamNameConstant.DOWNLOAD_SPEED);
        String unloadSpeed = request.getParameter(ParamNameConstant.UNLOAD_SPEED);
        HttpSession session = request.getSession();

        if (StringUtils.isNullOrEmpty(name, price, downloadSpeed, unloadSpeed)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.emptyFields");
            return CommandResult.redirect(CommandEnum.SHOW_TARIFFS_ADMIN.getCommandUrl());
        }

        if (StringUtils.lengthMoreThan(15, name) || StringUtils.lengthMoreThan(8, downloadSpeed, unloadSpeed)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.fieldsTooBig");
            return CommandResult.redirect(CommandEnum.SHOW_TARIFFS_ADMIN.getCommandUrl());
        }

        if (!StringUtils.regexMatch(PRICE_REGEX, price)
                || !StringUtils.regexMatch(INTEGER_REGEX, downloadSpeed, unloadSpeed)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.incorrectInformation");
            return CommandResult.redirect(CommandEnum.SHOW_TARIFFS_ADMIN.getCommandUrl());
        }

        BuilderFromRequest<Tariff> builder = new TariffBuilderFromRequest();
        Tariff tariff = builder.build(request);
        TariffService service = new TariffServiceImpl();
        service.save(tariff);
        session.setAttribute(ParamNameConstant.SUCCESSFUL, true);
        LOGGER.info("Tariff with id = " + tariff.getId() + "was successfully add");
        return CommandResult.redirect(CommandEnum.SHOW_TARIFFS_ADMIN.getCommandUrl());
    }
}
