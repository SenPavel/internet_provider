package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Traffic;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.helper.CommandHelper;
import com.epam.training.internetprovider.service.traffic.TrafficService;
import com.epam.training.internetprovider.service.traffic.TrafficServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Implementation of the <tt>Command</tt> interface for showTraffic command
 */

public class ShowTrafficCommand implements Command {

    private static final Long TRAFFIC_COUNT_IN_ONE_PAGE = 15L;

    /**
     * Method find traffic with limit and set their in request, also set attributes for pagination
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.forward with parameter {@link PageConstant#TRAFFIC}
     * @throws ServiceException if something happened with TrafficServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        TrafficService service = new TrafficServiceImpl();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ParamNameConstant.USER);
        Long trafficFieldsCount = service.getFieldsCount();
        Long rowFromNumber = CommandHelper.calculateAndSetAttrForPagination(request, TRAFFIC_COUNT_IN_ONE_PAGE, trafficFieldsCount);
        List<Traffic> traffics = service.findAllWithLimitByUserId(user.getId(), TRAFFIC_COUNT_IN_ONE_PAGE, rowFromNumber);
        request.setAttribute(ParamNameConstant.TRAFFICS, traffics);
        return CommandResult.forward(PageConstant.TRAFFIC);
    }
}
