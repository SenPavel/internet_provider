package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.Tariff;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.tariff.TariffService;
import com.epam.training.internetprovider.service.tariff.TariffServiceImpl;
import com.epam.training.internetprovider.service.user.UserService;
import com.epam.training.internetprovider.service.user.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Implementation of the <tt>Command</tt> interface for showUsers command
 */

public class ShowUsersCommand implements Command {

    /**
     * Method find all users and all tariffs and set their in request
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return CommandResult.forward with parameter {@link PageConstant#USERS}
     * @throws ServiceException if something happened with TariffServiceImpl or UserServiceImpl object
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        UserService userService = new UserServiceImpl();
        List<User> users = userService.findAll();
        request.setAttribute(ParamNameConstant.USERS, users);
        TariffService tariffService = new TariffServiceImpl();
        List<Tariff> tariffs = tariffService.findAll();
        request.setAttribute(ParamNameConstant.TARIFFS, tariffs);

        return CommandResult.forward(PageConstant.USERS);
    }
}
