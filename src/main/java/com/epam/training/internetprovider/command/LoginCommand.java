package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.constant.CommandEnum;
import com.epam.training.internetprovider.constant.PageConstant;
import com.epam.training.internetprovider.constant.ParamNameConstant;
import com.epam.training.internetprovider.entity.User;
import com.epam.training.internetprovider.exception.ServiceException;
import com.epam.training.internetprovider.service.payment.PaymentService;
import com.epam.training.internetprovider.service.payment.PaymentServiceImpl;
import com.epam.training.internetprovider.service.user.UserService;
import com.epam.training.internetprovider.service.user.UserServiceImpl;
import com.epam.training.internetprovider.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * Implementation of the <tt>Command</tt> interface for login command
 */


public class LoginCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(LoginCommand.class);
    private static final String LOGIN_PAGE = "/";

    /**
     * Method check parameters from HttpServletRequest and if they valid save user object in database
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return return CommandResult.forward with parameter {@link PageConstant#LOGIN}
     * if HttpServletRequest contain incorrect parameters otherwise
     * return CommandResult.redirect with parameter {@link CommandEnum#SHOW_MAIN}
     * @throws ServiceException if something happened with UserServiceImpl object
     */
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String login = request.getParameter(ParamNameConstant.LOGIN);
        String password = request.getParameter(ParamNameConstant.PASSWORD);

        HttpSession session = request.getSession(true);
        if (StringUtils.isNullOrEmpty(login, password)) {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.emptyFields");
            return CommandResult.redirect(LOGIN_PAGE);
        }

        UserService userService = new UserServiceImpl();
        Optional<User> optionalUser = userService.login(login, password);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            session.setAttribute(ParamNameConstant.USER, user);
            PaymentService paymentService = new PaymentServiceImpl();
            BigDecimal balance = paymentService.currentBalanceByUserId(user.getId());
            session.setAttribute(ParamNameConstant.BALANCE, balance.doubleValue());
            LOGGER.info("User with login: " + user.getLogin() + " successfully login");
        } else {
            session.setAttribute(ParamNameConstant.ERROR_MESSAGE, "error.incorrectLoginInfo");
            return CommandResult.redirect(LOGIN_PAGE);
        }

        return CommandResult.redirect(CommandEnum.SHOW_MAIN.getCommandUrl());

    }
}
