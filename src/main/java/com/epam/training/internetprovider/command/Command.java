package com.epam.training.internetprovider.command;

import com.epam.training.internetprovider.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *  The <tt>Command</tt> interface provides one method
 *  to perform some actions (like set attributes, validate parameters and etc)
 *  with <tt>HttpServletRequest</tt> and <tt>HttpServletResponse</tt> objects
 *  All command are in the enum CommandEnum
 *
 * @see com.epam.training.internetprovider.constant.CommandEnum
 */

public interface Command {

    /**
     * Perform actions with <tt>HttpServletRequest</tt> and <tt>HttpServletResponse</tt> objects
     * @param request to provide request information for HTTP servlets.
     * @param response to provide HTTP-specific functionality in sending a response.
     * @return <tt>CommandResult</tt> object which contains page path for redirect or forward
     * @throws ServiceException if something happened with service's objects
     */
    CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException;
}
