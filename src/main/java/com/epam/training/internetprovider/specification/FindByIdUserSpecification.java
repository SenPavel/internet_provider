package com.epam.training.internetprovider.specification;

import java.util.Arrays;
import java.util.List;

public class FindByIdUserSpecification implements Specification {

    private static final String SQL = "id_user = ?";
    private Long id;

    public FindByIdUserSpecification(Long id) {
        this.id = id;
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id);
    }

    @Override
    public String toSql() {
        return SQL;
    }
}
