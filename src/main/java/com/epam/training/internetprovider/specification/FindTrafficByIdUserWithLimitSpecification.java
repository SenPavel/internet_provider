package com.epam.training.internetprovider.specification;

import java.util.Arrays;
import java.util.List;

public class FindTrafficByIdUserWithLimitSpecification implements Specification {

    private static final String SQL = "id_user = ? ORDER BY start_session DESC LIMIT ? OFFSET ?";

    private Long id;
    private Long rowCount;
    private Long rowFrom;

    public FindTrafficByIdUserWithLimitSpecification(Long id, Long rowCount, Long rowFrom) {
        this.id = id;
        this.rowCount = rowCount;
        this.rowFrom = rowFrom;
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id, rowCount, rowFrom);
    }

    @Override
    public String toSql() {
        return SQL;
    }
}
