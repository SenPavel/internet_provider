package com.epam.training.internetprovider.specification;

import java.util.List;

public interface Specification {

    List<Object> getParameters();

    String toSql();
}
