package com.epam.training.internetprovider.specification;

import java.util.Arrays;
import java.util.List;

public class FindByUserLoginSpecification implements Specification {

    private static final String SQL = "login = ?";

    private String login;

    public FindByUserLoginSpecification(String login) {
        this.login = login;
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(login);
    }

    @Override
    public String toSql() {
        return SQL;
    }
}
