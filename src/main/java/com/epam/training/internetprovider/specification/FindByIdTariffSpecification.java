package com.epam.training.internetprovider.specification;

import java.util.Arrays;
import java.util.List;

public class FindByIdTariffSpecification implements Specification {

    private static final String SQL = "id_tariff = ?";
    private Long id;

    public FindByIdTariffSpecification(Long id) {
        this.id = id;
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id);
    }

    @Override
    public String toSql() {
        return SQL;
    }
}
