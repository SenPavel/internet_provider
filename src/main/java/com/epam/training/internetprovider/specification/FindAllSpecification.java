package com.epam.training.internetprovider.specification;

import java.util.Collections;
import java.util.List;

public class FindAllSpecification implements Specification {

    private static final String TRUE = "true";

    @Override
    public List<Object> getParameters() {
        return Collections.emptyList();
    }

    @Override
    public String toSql() {
        return TRUE;
    }
}
