package com.epam.training.internetprovider.specification;

import java.util.Arrays;
import java.util.List;

public class LoginAndPasswordSpecification implements Specification {

    private static final String SQL = "login = ? and password = ?";
    private final String login;
    private final String password;

    public LoginAndPasswordSpecification(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(login, password);
    }

    @Override
    public String toSql() {
        return SQL;
    }
}
