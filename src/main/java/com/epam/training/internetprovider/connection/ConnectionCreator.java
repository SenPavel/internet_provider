package com.epam.training.internetprovider.connection;

import com.epam.training.internetprovider.exception.ConnectionCreatorException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Class for reading the properties file for creating a database connection
 */

public class ConnectionCreator {

    private static final String PATH = "/db.properties";
    private static final String URL = "url";
    private static final String USER = "user";
    private static final String PASSWORD = "password";

    /**
     *
     * @return a database connection
     * @throws ConnectionCreatorException if the wrong path to the properties file
     * or something happened with database
     */
    public Connection create() throws ConnectionCreatorException {
        Properties properties = new Properties();
        Connection connection;
        try {
            InputStream stream = ConnectionCreator.class.getResourceAsStream(PATH);
            properties.load(stream);
            String url = properties.getProperty(URL);
            String name = properties.getProperty(USER);
            String password = properties.getProperty(PASSWORD);
            connection = DriverManager.getConnection(url, name, password);
        } catch (SQLException | IOException ex) {
            throw new ConnectionCreatorException(ex);
        }
        return connection;
    }
}
