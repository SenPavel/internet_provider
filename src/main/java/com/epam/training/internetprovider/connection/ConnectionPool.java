package com.epam.training.internetprovider.connection;

import com.epam.training.internetprovider.exception.ConnectionCreatorException;
import com.epam.training.internetprovider.exception.ConnectionPoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class creates connections to the database, stores them and distributes them as needed.
 * Since connecting to a database takes a lot of time,
 * the best way is to create connections in advance and give them when needed.
 */

public class ConnectionPool {

    private static final Integer DEFAULT_POOL_SIZE = 5;
    private static final Lock LOCK = new ReentrantLock();
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);

    private static ConnectionPool instance = null;
    private static AtomicBoolean initialized = new AtomicBoolean(false);

    private ConnectionCreator creator = new ConnectionCreator();

    /**
     * Queue to store connections
     */
    private BlockingQueue<Connection> availableConnections = new ArrayBlockingQueue<>(DEFAULT_POOL_SIZE);

    /**
     * Constructor create connections and put their in queue
     */
    private ConnectionPool() {

    }

    public static ConnectionPool getInstance() {
        if (!initialized.get()) {
            try {
                LOCK.lock();
                if (!initialized.get()) {
                    instance = new ConnectionPool();
                    initialized.set(true);
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    /**
     * Initializing connection pool by creating connection and add they to queue
     */
    public void initialize() {
        try {
            LOGGER.info("Initializing connection pool. Pool size: " + DEFAULT_POOL_SIZE);
            Class.forName("com.mysql.cj.jdbc.Driver");
            for (int i = 0; i < DEFAULT_POOL_SIZE; i++) {
                Connection connection = creator.create();
                availableConnections.add(connection);
            }
        } catch (ClassNotFoundException | ConnectionCreatorException ex) {
            throw new ConnectionPoolException(ex);
        }
    }

    /**
     * @return a free connection
     */
    public Connection getConnection() {
        Connection connection;
        try {
            connection = availableConnections.take();
        } catch (InterruptedException ex) {
            throw new ConnectionPoolException(ex);
        }
        return connection;
    }

    /**
     * Return connection in queue
     * @param connection
     */
    public void returnConnection(Connection connection) {
        try {
            if (!connection.getAutoCommit()) {
                connection.setAutoCommit(true);
            }
            availableConnections.put(connection);
        } catch (InterruptedException | SQLException ex) {
            throw new ConnectionPoolException(ex);
        }
    }

    /**
     * Close connection pool
     */
    public void destroy() {
        try {
            if (availableConnections.size() == DEFAULT_POOL_SIZE) {
                for (int i = 0; i < DEFAULT_POOL_SIZE; i++) {
                    availableConnections.take().close();
                }
            }
            LOGGER.info("Destroying connection pool");
        } catch (SQLException | InterruptedException ex) {
            throw new ConnectionPoolException(ex);
        }
    }
}
