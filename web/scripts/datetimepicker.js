$('#datetime').datepicker({
    timepicker: true,
    timeFormat: "hh:ii:00",
    dateFormat: "yyyy-mm-dd",
    position: "top right",
    minDate: new Date(),
    minHours: new Date(),

});
$('#datetime').data('datepicker');