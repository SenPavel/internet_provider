function changeForm(form, button) {
    let accountButton = $("#accountButton");
    let securityButton = $("#securityButton");
    let accountForm = $("#accountForm");
    let securityForm = $("#securityForm");

    accountButton.css("background", "grey");
    securityButton.css("background-color", "grey");
    accountForm.css("display", "none");
    securityForm.css("display", "none");

    $("#" + form).css("display", "block");
    $("#" + button).css("background-color", "rgba(255, 255, 255, 0.70)");
}

function checkAccountInfoInputsEmpty(message) {
    var emptyInputs = 0;
    var elements = $('#accountForm input[type=text]');
    $(elements).each(function() {
        if ($.trim($(this).val()).length == 0) {
            emptyInputs++;
        }
    });
    if (elements.length == emptyInputs) {
        $(elements).each(function() {
            $(this).attr("required", true);
            this.setCustomValidity(message);
        });
    } else {
        $(elements).each(function() {
            $(this).removeAttr("required");
            this.setCustomValidity('');
        });
    }
}

function checkAccountSecurityInputsEmpty(message) {
    var login = $('input[name=login]');
    var oldPassword = $('input[name=old_password]');
    var password = $('input[name=password]');
    if ($.trim($(login).val()).length == 0 && $.trim($(oldPassword).val()).length == 0) {
        $(login).attr("required", true);
        login.setCustomValidity(message);
    } else {
        $(login).attr("required", false);
        login.setCustomValidity("");
    }

    if ($.trim($(oldPassword).val()).length != 0 && $.trim($(password).val()).length == 0) {
        $(password).attr("required", true);
        password.setCustomValidity(message);
    } else {
        $(password).attr("required", false);
        password.setCustomValidity("");
    }
}

$(document).ready(function() {
    var oldPassword = $("#old_password");
    var password = $("#password");
    $(oldPassword).on("input", function () {
        if (($.trim($(oldPassword).val()).length != 0)) {
            $(password).prop("disabled", false);
        } else {
            $(password).attr("disabled", true);
        }
    });
});