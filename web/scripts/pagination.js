$(document).ready(function() {
    const urlParams = new URLSearchParams(window.location.search);
    const page = urlParams.get('page');
    if (!page) {
        $('#paginate li:contains("1")').addClass('selected');
    } else {
        $('#paginate li:contains(' + page + ')').addClass('selected');
    }
});

$('#paginate li').click(function() {
    var $item = $('li.selected').text();
    var $h = $('#paginate li').eq(-2).text();
    $('#paginate li').removeClass('selected');
    if ($(this).text() == "<<") {
        if ($item != "1") {
            $('#paginate li:contains(' + --$item + ')').addClass('selected');
        } else {
            $('#paginate li:contains(' + $item + ')').addClass('selected');
        }
    } else if ($(this).text() == ">>") {
        if ($item != $h) {
            $('#paginate li:contains(' + ++$item + ')').addClass('selected');
        } else {
            $('#paginate li:contains(' + $item + ')').addClass('selected');
        }
    } else {
        $(this).addClass('selected');
    }
});

