function showChangeForm(containerElement, formElement, firstId, secondId) {
    var container = $("#" + containerElement);
    var form = $("#" + formElement);
    if (typeof firstId != "undefined") {
        $("#" + form.attr("id") + " input[type=hidden]").val(firstId);
        let i = 0;
        $("#info" + firstId).find("td").each(function () {
            var value = $(this).html();
            $("#" + container.attr("id") + " input[type=text]").eq(i).val(value);
            i++;
        });
        $("#" + form.attr("id") + " select").children("option").eq(secondId).attr("selected", "selected");
    }
    container.show();
}


function closeChangeForm(containerElement, formElement) {
    var container = $("#" + containerElement);
    var form = $("#" + formElement);
    container.hide();
    form[0].reset();
}

$(document).mouseup(function(e) {
    var container = $(".form-change-container");
    var datetimepicker = $(".form-change-container").find(".datepicker-here");
    var datepicker = $(".datepicker");
    if (datetimepicker == null) {
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.hide();
            $(".form-change-container .form-change")[0].reset();
        }
    } else {
        if (!container.is(e.target) && container.has(e.target).length === 0
            && !datepicker.is(e.target) && datepicker.has(e.target).length === 0) {
            container.hide();
            $(".form-change-container .form-change")[0].reset();
        }
    }
});