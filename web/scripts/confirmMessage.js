function showMessage(message, link) {
    if ($.cookie("tariffEmptyMessage") == null) {
        if (window.confirm(message)) {
            window.location.href = link;
        }
        $.cookie("tariffEmptyMessage", "shown");
    }
}

function deleteShowMessageCookie() {
    $.removeCookie("tariffEmptyMessage");
}