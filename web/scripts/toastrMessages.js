function errorToastr(message) {
    toastr.options.progressBar = true;
    toastr.error(message) ;
}

function successToastr(message) {
    toastr.options.progressBar = true;
    toastr.success(message) ;
}