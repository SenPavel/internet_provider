
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="с" uri="http://java.sun.com/jsp/jstl/core" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<c:set var="page" value="${pageContext.request.queryString}"/>
<html lang="${sessionScope.lang}">
<head>
    <link href="${pageContext.request.contextPath}/styles/fonts/fontawesome-free-5.6.1-web/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../styles/menu.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/menu-media.css"/>
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery-3.3.1.min.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery.cookie.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/confirmMessage.js"/>'></script>
    <title></title>
</head>
<body>
<div class="container-menu">
    <div class="menu-head">
        <div class="parent-lang">
            <ul class="lang">
                <c:choose>
                    <c:when test="${not empty pageContext.request.queryString}">
                        <li>
                            <a href="${pageContext.servletContext.contextPath}/controller?${page}&sessionLocale=ru">
                                <img src="../../images/russian-flag-icon.png" alt="RU">
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.servletContext.contextPath}/controller?${page}&sessionLocale=en">
                                <img src="../../images/united-kingdom-flag-icon.png" alt="EN">
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.servletContext.contextPath}/controller?${page}&sessionLocale=be">
                                <img src="../../images/Belarussian-flag-icon.png" alt="BE">
                            </a>
                        </li>

                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="?sessionLocale=ru">
                                <img src="../../images/russian-flag-icon.png" alt="RU">
                            </a>
                        </li>
                        <li>
                            <a href="?sessionLocale=en">
                                <img src="../../images/united-kingdom-flag-icon.png" alt="EN">
                            </a>
                        </li>
                        <li>
                            <a href="?sessionLocale=be">
                                <img src="../../images/Belarussian-flag-icon.png" alt="BY">
                            </a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>
            <div class="logout" onclick="deleteShowMessageCookie()">
                <a href="${pageContext.servletContext.contextPath}/controller?command=logout">
                    <i class="fas fa-sign-out-alt fa-2x "></i>
                </a>
            </div>
        </div>
        <div class="user-image"><img src="../../images/user-icon.jpg"></div>
        <div><span>${sessionScope.user.firstName} ${sessionScope.user.lastName}</span></div>
        <c:choose>
            <c:when test="${sessionScope.user.admin}">
                <div><span><fmt:message key="label.admin"/></span></div>
            </c:when>
            <c:otherwise>
                <div><span><fmt:message key="label.balance"/>: ${sessionScope.balance}</span></div>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="menu-center">
        <ul>
            <li>
                <a href="${pageContext.servletContext.contextPath}/controller?command=showMain">
                    <i class="fas fa-home"></i> <span><fmt:message key="title.main"/></span>
                </a>
            </li>
            <li>
                <a href="${pageContext.servletContext.contextPath}/controller?command=showInformation">
                    <i class="fas fa-user-alt"></i> <span><fmt:message key="label.account"/></span>
                </a>
            </li>
            <c:choose>
                <c:when test="${not sessionScope.user.admin}">
                    <li>
                        <a href="${pageContext.servletContext.contextPath}/controller?command=showPayments">
                            <i class="fas fa-hand-holding-usd"></i> <span><fmt:message key="label.payments"/></span>
                        </a>
                    </li>
                    <li>
                        <a href="${pageContext.servletContext.contextPath}/controller?command=showTariffs">
                            <i class="fas fa-signal"></i> <span><fmt:message key="title.tariffs"/></span>
                        </a>
                    </li>
                    <li>
                        <a href="${pageContext.servletContext.contextPath}/controller?command=showTraffic">
                            <i class="fas fa-wifi"></i> <span><fmt:message key="label.traffic"/></span>
                        </a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a href="${pageContext.servletContext.contextPath}/controller?command=showUsers">
                            <i class="fas fa-user-alt"></i> <span><fmt:message key="title.users"/></span>
                        </a>
                    </li>
                    <li>
                        <a href="${pageContext.servletContext.contextPath}/controller?command=showTariffsAdmin">
                            <i class="fas fa-signal"></i> <span><fmt:message key="title.tariffs"/></span>
                        </a>
                    </li>
                    <li>
                        <a href="${pageContext.servletContext.contextPath}/controller?command=showDiscounts">
                            <i class="fas fa-percentage"></i> <span><fmt:message key="title.discounts"/></span>
                        </a>
                    </li>
                </c:otherwise>
            </c:choose>
            <li>
                <a href="${pageContext.servletContext.contextPath}/controller?command=showSettings">
                    <i class="fas fa-cog"></i> <span><fmt:message key="title.settings"/></span>
                </a>
            </li>
        </ul>
    </div>
</div>
</body>
</html>