<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="title.tariffs"/> </title>
    <link rel="stylesheet" type="text/css" href="../../styles/table.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/toastr.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/table-media.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/form-change.css"/>
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery-3.3.1.min.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastr.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastrMessages.js"/>'></script>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/menu.jsp"/>
<div class="bg-image-tariffs"></div>
<button class="button-add" onclick=showChangeForm("form-change-tariff-container","form-change-tariff")>Add tariff</button>
<div class="table-container">
    <div class="table-container-child">
        <table>
            <thead>
            <tr>
                <th scope="col"><fmt:message key="label.name"/></th>
                <th scope="col"><fmt:message key="label.price"/></th>
                <th scope="col"><fmt:message key="label.downloadSpeed"/></th>
                <th scope="col"><fmt:message key="label.unloadSpeed"/></th>
                <th scope="col"><fmt:message key="label.discountPrice"/></th>
                <th scope="col"><fmt:message key="label.discount"/></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.tariffs}" var="tariff">
                <tr id="info${tariff.id}">
                    <td data-label="<fmt:message key="label.name"/>">${tariff.name}</td>
                    <td data-label="<fmt:message key="label.price"/>">${tariff.price}</td>
                    <td data-label="<fmt:message key="label.downloadSpeed"/>">${tariff.downloadSpeed}</td>
                    <td data-label="<fmt:message key="label.unloadSpeed"/>">${tariff.unloadSpeed}</td>
                    <td data-label="<fmt:message key="label.discountPrice"/>">${tariff.discountPrice}</td>
                    <c:choose>
                        <c:when test="${tariff.idDiscount eq 0}">
                            <td data-label="Discount">
                                <fmt:message key="label.noDiscount"/>
                            </td>
                        </c:when>
                        <c:otherwise>
                            <c:forEach items="${requestScope.discounts}" var="discount">
                                    <c:if test="${tariff.idDiscount eq discount.id}">
                                        <td data-label="Discount">${discount.name}</td>
                                    </c:if>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    <td data-label="" height="40px">
                        <button onclick=showChangeForm("form-change-tariff-container","form-change-tariff","${tariff.id}","${tariff.idDiscount}")>
                            <fmt:message key="button.change"/>
                        </button>
                    </td>
                    <td data-label="" height="40px">
                        <form method="post" action="${pageContext.servletContext.contextPath}/controller?command=deleteTariff&id_tariff=${tariff.id}">
                            <button onclick="return confirm('<fmt:message key="message.confirmDeleteTariff"/> \'${tariff.name}\'?')" >
                                <fmt:message key="button.delete"/>
                            </button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div class="form-change-container" id="form-change-tariff-container">
    <form class="form-change" name="form-change-tariff" id="form-change-tariff" method="post" action="${pageContext.servletContext.contextPath}/controller?command=updateTariffs">
        <i class="fas fa-times" id="close" style="float: right" onclick=closeChangeForm("form-change-tariff-container","form-change-tariff")></i>
        <label><fmt:message key="label.name"/>
            <input type="text" name="name" required="required" maxlength="15"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.emptyFields'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="label.price"/>
            <input type="text" name="price" required="required" pattern="^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.incorrectPrice'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="label.downloadSpeed"/>
            <input type="text" name="download_speed" required="required" pattern="^[1-9][0-9]*$" maxlength="8"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.incorrectSpeed'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="label.unloadSpeed"/>
            <input type="text" name="unload_speed" required="required" pattern="^[1-9][0-9]*$" maxlength="8"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.incorrectSpeed'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="label.discount"/>
            <select name="id_discount">
                <option value="0">Not selected</option>
                <c:forEach items="${requestScope.discounts}" var="discount">
                    <option title="discount: ${discount.discount}%" value="${discount.id}">${discount.name}</option>
                </c:forEach>
            </select>
        </label>
        <input type="hidden" id="id_tariff" name="id_tariff">
        <input type="submit"  value="<fmt:message key="button.update"/>">
    </form>
</div>
<c:if test="${sessionScope.successful eq true}">
    <script type="text/javascript">
        successToastr("<fmt:message key='message.successfullyUpdateTariffs'/>");
    </script>
    <c:remove var="successful" scope="session"/>
</c:if>
<c:if test="${not empty sessionScope.errorMessage}">
    <script type="text/javascript">
        errorToastr("<fmt:message key='${sessionScope.errorMessage}'/>");
    </script>
    <c:remove var="errorMessage" scope="session"/>
</c:if>
<script type="text/javascript" src='<c:url value="../../scripts/form-change.js"/>'></script>
</body>
</html>
