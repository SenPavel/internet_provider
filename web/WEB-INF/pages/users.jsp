<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="title.users"/></title>
    <link rel="stylesheet" type="text/css" href="../../styles/toastr.css">
    <link rel="stylesheet" type="text/css" href="../../styles/table.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/form-change.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/table-media.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/background.css"/>
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery-3.3.1.min.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastr.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastrMessages.js"/>'></script>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/menu.jsp"/>
<div class="bg-image"></div>
<button class="button-add" onclick=showChangeForm("form-change-user-container","form-change-user")>Add user</button>
<div class="table-container">
    <div class="table-container-child">
        <table>
            <thead>
            <tr>
                <th scope="col"><fmt:message key="label.firstName"/></th>
                <th scope="col"><fmt:message key="label.lastName"/></th>
                <th scope="col"><fmt:message key="label.surname"/></th>
                <th scope="col"><fmt:message key="label.status"/></th>
                <th scope="col"><fmt:message key="label.role"/></th>
                <th scope="col"><fmt:message key="label.login"/></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.users}" var="user">
                <c:if test="${sessionScope.user.id ne user.id}">
                    <tr>
                        <td data-label="<fmt:message key="label.firstName"/>">${user.firstName}</td>
                        <td data-label="<fmt:message key="label.lastName"/>">${user.lastName}</td>
                        <td data-label="<fmt:message key="label.surname"/>">${user.surname}</td>
                        <c:choose>
                            <c:when test="${user.blocked}">
                                <td data-label="<fmt:message key="label.status"/>"><fmt:message key="label.blocked"/> </td>
                            </c:when>
                            <c:otherwise>
                                <td data-label="<fmt:message key="label.status"/>"><fmt:message key="label.notBlocked"/></td>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${user.admin}">
                                <td data-label="<fmt:message key="label.role"/>"><fmt:message key="label.admin"/> </td>
                            </c:when>
                            <c:otherwise>
                                <td data-label="<fmt:message key="label.role"/>"><fmt:message key="label.user"/></td>
                            </c:otherwise>
                        </c:choose>
                        <td data-label="<fmt:message key="label.login"/>">${user.login}</td>
                        <td data-label="" height="40px">
                            <c:choose>
                                <c:when test="${user.blocked}">
                                    <form method="post" action="${pageContext.servletContext.contextPath}/controller?command=blockUser&is_blocked=false">
                                        <input type="hidden" name="id_user" value="${user.id}">
                                        <button type="submit" onclick="return confirm('<fmt:message key="message.confirmUnblock"/> \'${user.firstName}\'?')">
                                            <fmt:message key="button.unblock"/>
                                        </button>
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <form method="post" action="${pageContext.servletContext.contextPath}/controller?command=blockUser&is_blocked=true">
                                        <input type="hidden" name="id_user" value="${user.id}">
                                        <button type="submit" onclick="return confirm('<fmt:message key="message.confirmBlock"/> \'${user.firstName}\'?')">
                                            <fmt:message key="button.block"/>
                                        </button>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div class="form-change-container" id="form-change-user-container">
    <form class="form-change" id="form-change-user" method="post" action="${pageContext.servletContext.contextPath}/controller?command=addUser">
        <i class="fas fa-times" id="close" onclick=closeChangeForm("form-change-user-container","form-change-user"); style="float: right"></i>
        <label><fmt:message key="label.firstName"/>
            <input type="text" required name="first_name" maxlength="35" pattern="[\p{L}\s]+"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.emptyFields'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="label.lastName"/>
            <input type="text" required name="last_name" maxlength="35" pattern="[\p{L}\s]+"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.emptyFields'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="label.surname"/>
            <input type="text" required name="surname" maxlength="35" pattern="[\p{L}\s]+"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.emptyFields'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="title.tariffs"/>
            <select name="id_tariff">
                <c:forEach items="${requestScope.tariffs}" var="tariff">
                    <option value="${tariff.id}">${tariff.name}</option>
                </c:forEach>
            </select>
        </label>
        <label><fmt:message key="label.role"/>
            <select name="is_admin">
                <option value="true"><fmt:message key="label.admin"/></option>
                <option value="false"><fmt:message key="label.user"/></option>
            </select>
        </label>
        <label><fmt:message key="label.login"/>
            <input type="text" required name="login" maxlength="20"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.emptyFields'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="label.password"/>
            <input type="text" required name="password" maxlength="20"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.emptyFields'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <input type="submit" value="<fmt:message key="button.add"/>">
    </form>
</div>
<c:if test="${sessionScope.successful eq true}">
    <script type="text/javascript">
        successToastr("<fmt:message key='message.successfullyAddUser'/>");
    </script>
    <c:remove var="successful" scope="session"/>
</c:if>
<c:if test="${not empty sessionScope.errorMessage}">
    <script type="text/javascript">
        errorToastr("<fmt:message key='${sessionScope.errorMessage}'/>");
    </script>
    <c:remove var="errorMessage" scope="session"/>
</c:if>
<script type="text/javascript" src='<c:url value="../../scripts/form-change.js"/>'></script>
</body>
</html>
