<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<c:set var="page" value="${pageContext.request.queryString}"/>
<html lang="${sessionScope.lang}">
<head>
    <link rel="stylesheet" type="text/css" href="../../styles/login.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/login-media.css"/>
    <title>Login</title>
</head>
<body>
<div class="bg-image"></div>
<div class="parent">
    <div class="child">
        <ul class="lang-login">
            <c:choose>
                <c:when test="${not empty pageContext.request.queryString}">
                    <li>
                        <a href="${pageContext.servletContext.contextPath}/controller?${page}&sessionLocale=ru">
                            <img src="../../images/russian-flag-icon.png" alt="RU">
                        </a>
                    </li>
                    <li>
                        <a href="${pageContext.servletContext.contextPath}/controller?${page}&sessionLocale=en">
                            <img src="../../images/united-kingdom-flag-icon.png" alt="EN">
                        </a>
                    </li>
                    <li>
                        <a href="${pageContext.servletContext.contextPath}/controller?${page}&sessionLocale=be">
                            <img src="../../images/Belarussian-flag-icon.png" alt="BE">
                        </a>
                    </li>

                </c:when>
                <c:otherwise>
                    <li>
                        <a href="?sessionLocale=ru">
                            <img src="../../images/russian-flag-icon.png">
                        </a>
                    </li>
                    <li>
                        <a href="?sessionLocale=en">
                            <img src="../../images/united-kingdom-flag-icon.png">
                        </a>
                    </li>
                    <li>
                        <a href="?sessionLocale=be">
                            <img src="../../images/Belarussian-flag-icon.png">
                        </a>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
        <c:if test="${not empty sessionScope.errorMessage}">
            <div class="error"><fmt:message key="${sessionScope.errorMessage}"/></div>
            <c:remove var="errorMessage" scope="session"/>
        </c:if>
        <form action="${pageContext.servletContext.contextPath}/controller?command=login" method="post">
            <p><input class="input-text" type="text" name="login" placeholder="<fmt:message key="label.login"/>" required="required"></p>
            <p><input class="input-text" type="password" placeholder="<fmt:message key="label.password"/>" name="password" required="required"></p><br>
            <p><input class="button" type="submit" value="<fmt:message key="button.login"/>"></p>
        </form>
    </div>
</div>

</body>
</html>
