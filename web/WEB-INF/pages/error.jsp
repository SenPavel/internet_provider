
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<c:set var="page" value="${pageContext.request.queryString}"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="title.error"/></title>
    <link rel="stylesheet" type="text/css" href="../../styles/error.css"/>
</head>
<body>
<div class="bg-image"></div>
<div class="parent">
    <div class="child">
        <h1><fmt:message key="label.somethingWentWrong"/> </h1>
        <span><fmt:message key="label.errorMessage"/>: </span>
        <div style="background: rgba(255, 0, 0, 0.40)">
            <span>${requestScope.errorMessage}</span>
        </div>
        <a href="${pageContext.servletContext.contextPath}/controller?command=showMain">
            <span><fmt:message key="button.backToMain"/> </span>
        </a>
    </div>
</div>
</body>
</html>
