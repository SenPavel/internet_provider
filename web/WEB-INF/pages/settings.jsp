<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="title.settings"/></title>
    <link rel="stylesheet" href="../../styles/toastr.css">
    <link rel="stylesheet" href="../../styles/settings.css">
    <link rel="stylesheet" href="../../styles/media/settings-media.css">
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery-3.3.1.min.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/settings.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastr.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastrMessages.js"/>'></script>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/menu.jsp"/>
<div class="bg-image-settings"></div>
<div class="settings-container">
    <ul>
        <li id="accountButton" onclick=changeForm("accountForm","accountButton")><fmt:message key="label.personalInfo"/></li>
        <li id="securityButton" onclick=changeForm("securityForm","securityButton")><fmt:message key="label.securityInfo"/></li>
    </ul>
    <div class="settings-container-child">
        <form id="accountForm" action="${pageContext.servletContext.contextPath}/controller?command=updatePersonalInformation" method="post">
            <label><fmt:message key="label.firstName"/>
                <input type="text" name="first_name" pattern="[\p{L}\s]+" maxlength="35"
                       oninput="this.setCustomValidity('')">
            </label>
            <label><fmt:message key="label.lastName"/>
                <input type="text" name="last_name" pattern="[\p{L}\s]+" maxlength="35"
                       oninput="this.setCustomValidity('')">
            </label>
            <label><fmt:message key="label.surname"/>
                <input type="text" name="surname" pattern="[\p{L}\s]+" maxlength="35"
                       oninput="this.setCustomValidity('')">
            </label>
            <input name="id_user" type="hidden" value="${sessionScope.user.id}">
            <input type="submit" id="accountSubmit" value="<fmt:message key="label.ACCEPT"/> "
                   onclick="checkAccountInfoInputsEmpty('<fmt:message key='error.allFieldsEmpty'/>')">
        </form>
        <form id="securityForm" action="${pageContext.servletContext.contextPath}/controller?command=updateSecurityInformation" method="post">
            <label><fmt:message key="label.login"/>
                <input type="password" name="login" maxlength="20">
            </label>
            <label><fmt:message key="label.password"/>
                <input type="password" id="old_password" name="old_password" placeholder="<fmt:message key="label.oldPassword"/>" maxlength="20">
            </label>
            <label>
                <input type="password" id="password" name="password" disabled="" required="" placeholder="<fmt:message key="label.newPassword"/>" maxlength="20">
            </label>
            <input name="id_user" type="hidden" value="${sessionScope.user.id}">
            <input type="submit" value="<fmt:message key="label.ACCEPT"/> "
                   onclick="checkAccountSecurityInputsEmpty('<fmt:message key='error.allFieldsEmpty'/>')">
        </form>
    </div>
</div>
<c:if test="${sessionScope.successful eq true}">
    <script type="text/javascript">
        successToastr("<fmt:message key='message.successfullyUpdateUserInformation'/>");
    </script>
    <c:remove var="successful" scope="session"/>
</c:if>
<c:if test="${not empty sessionScope.errorMessage}">
    <script type="text/javascript">
        errorToastr("<fmt:message key='${sessionScope.errorMessage}'/>");
    </script>
    <c:remove var="errorMessage" scope="session"/>
</c:if>
</body>
</html>
