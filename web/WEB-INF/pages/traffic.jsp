<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="m" uri="http://www.tagLib.com"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="label.traffic"/></title>
    <link rel="stylesheet" type="text/css" href="../../styles/traffic.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/traffic-media.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/table.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/table-media.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/pagination.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/pagination-media.css"/>
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery-3.3.1.min.js"/>'></script>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/menu.jsp"/>
<div class="bg-image-tariffs"></div>
<div class="traffic-container">
    <div class="table-container">
        <div class="table-container-child">
            <table>
                <thead>
                <tr>
                    <th scope="col"><fmt:message key="label.startSession"/></th>
                    <th scope="col"><fmt:message key="label.incomingTraffic"/></th>
                    <th scope="col"><fmt:message key="label.outcomingTraffic"/></th>
                    <th scope="col"><fmt:message key="label.endSession"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.traffics}" var="traffic">
                    <tr>
                        <td data-label="<fmt:message key="label.startSession"/>">
                            <fmt:formatDate type="both" timeStyle="short" value="${traffic.startSession}"/></td>
                        <td data-label="<fmt:message key="label.incomingTraffic"/>">${traffic.incoming}</td>
                        <td data-label="<fmt:message key="label.outcomingTraffic"/>">${traffic.outcoming}</td>
                        <td data-label="<fmt:message key="label.endSession"/>">
                            <fmt:formatDate type="both" timeStyle="short" value="${traffic.endSession}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <m:pagination link="${pageContext.servletContext.contextPath}/controller?command=showTraffic" currentPage="${param.page}" pagesCount="${requestScope.pagesCount}"/>
        </div>
    </div>
</div>
</body>
<script type="text/javascript src='<c:url value="../../scripts/pagination.js"/>'</script>
</html>