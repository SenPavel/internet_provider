<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="title.discounts"/></title>
    <link rel="stylesheet" type="text/css" href="../../styles/table.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/toastr.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/table-media.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/form-change.css"/>`
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery-3.3.1.min.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/datepicker.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastr.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastrMessages.js"/>'></script>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/menu.jsp"/>
<div class="bg-image-tariffs"></div>
<button class="button-add" onclick=showChangeForm("form-change-discount-container","form-change-discount")>Add discount</button>
<div class="table-container">
    <div class="table-container-child">
        <table>
            <thead>
            <tr>
                <th scope="col"><fmt:message key="label.name"/></th>
                <th scope="col"><fmt:message key="label.discount"/></th>
                <th scope="col"><fmt:message key="label.expirationDate"/></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.discounts}" var="discount">
                <tr id="info${discount.id}">
                    <td data-label="<fmt:message key="label.name"/>">${discount.name}</td>
                    <td data-label="<fmt:message key="label.discount"/>">${discount.discount}</td>
                    <td data-label="<fmt:message key="label.expirationDate"/>">
                        <fmt:formatDate type="both" value="${discount.expirationDate}"/>
                    </td>
                    <td data-label="" height="40px">
                        <button onclick=showChangeForm("form-change-discount-container","form-change-discount","${discount.id}")>
                            <fmt:message key="button.change"/>
                        </button>
                    </td>
                    <td data-label="" height="40px">
                        <form method="post" action="${pageContext.servletContext.contextPath}/controller?command=deleteDiscount&id_discount=${discount.id}">
                            <button type="submit" id="btn" onclick="return confirm('<fmt:message key="message.confirmDeleteDiscount"/> \'${discount.name}\'')">
                                <fmt:message key="button.delete"/>
                            </button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<div id="form-change-discount-container" class="form-change-container">
    <form class="form-change" id="form-change-discount" name="form-change-discount" method="post" action="${pageContext.servletContext.contextPath}/controller?command=updateDiscounts">
        <i class="fas fa-times" style="float: right" onclick=closeChangeForm("form-change-discount-container","form-change-discount")></i>
        <label><fmt:message key="label.name"/>
            <input type="text" name="name" required="required" maxlength="15"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.emptyFields'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="label.discount"/>, %
            <input type="text" name="discount" required="required" pattern="^[1-9][0-9]?$|^100$"
                   oninvalid="this.setCustomValidity('<fmt:message key='error.incorrectPercent'/>')"
                   oninput="this.setCustomValidity('');">
        </label>
        <label><fmt:message key="label.expirationDate"/>
            <input type="text" class="datepicker-here" id="datetime" name="expiration_date" data-language="${sessionScope.lang}"
                   pattern="[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]" required="required">
        </label>
        <input type="hidden" id="id_discount" name="id_discount">
        <input type="submit"  value="<fmt:message key="button.update"/>">
    </form>
</div>
<c:if test="${sessionScope.successful eq true}">
    <script type="text/javascript">
        successToastr("<fmt:message key='message.successfullyUpdateDiscounts'/>");
    </script>
    <c:remove var="successful" scope="session"/>
</c:if>
<c:if test="${not empty sessionScope.errorMessage}">
    <script type="text/javascript">
        errorToastr("<fmt:message key='${sessionScope.errorMessage}'/>");
    </script>
    <c:remove var="errorMessage" scope="session"/>
</c:if>
<script type="text/javascript" src='<c:url value="../../scripts/datetimepicker.js"/>'></script>
<script type="text/javascript" src='<c:url value="../../scripts/form-change.js"/>'></script>
</body>
</html>
