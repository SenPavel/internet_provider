<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="title.information"/> </title>
    <link rel="stylesheet" type="text/css" href="../../styles/information.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/information-media.css"/>
</head>
<header>

</header>
<body>
<jsp:include page="/WEB-INF/fragments/menu.jsp"/>   
<div class="bg-image-information"></div>
<div class="information-container">
    <div class="information-container-child">
        <table>
            <tr>
                <td><fmt:message key="label.lastName"/></td>
                <td>${sessionScope.user.lastName}</td>
            </tr>
            <tr>
                <td><fmt:message key="label.firstName"/> </td>
                <td>${sessionScope.user.firstName}</td>
            </tr>
            <tr>
                <td><fmt:message key="label.surname"/> </td>
                <td>${sessionScope.user.surname}</td>
            </tr>
            <tr>
                <td><fmt:message key="label.login"/> </td>
                <td>${sessionScope.user.login}</td>
            </tr>
            <c:if test="${not sessionScope.user.admin}">
                <tr>
                    <td><fmt:message key="label.balance"/> </td>
                    <td>${sessionScope.balance}</td>
                </tr>
                <tr>
                    <td><fmt:message key="label.tariff"/></td>
                    <c:choose>
                        <c:when test="${not empty sessionScope.user.idTariff}">
                            <td>${requestScope.tariff.name}</td>
                        </c:when>
                        <c:otherwise>
                            <td><fmt:message key="message.notSelectTariff"/> </td>
                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:if>
        </table>
    </div>
</div>
</body>
</html>
