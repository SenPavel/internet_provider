<%@ taglib prefix="с" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="title.main"/></title>
    <link rel="stylesheet" type="text/css" href="../../styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/main-media.css"/>
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery-3.3.1.min.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery.cookie.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/confirmMessage.js"/>'></script>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/menu.jsp"/>
<div class="bg-image-main"></div>
<div class="main-container">
    <a href="${pageContext.servletContext.contextPath}/controller?command=showInformation">
        <div class="main-container-child">
            <i class="fas fa-user-alt fa-3x"></i>
            <span><fmt:message key="label.account"/></span>
        </div>
    </a>
    <с:choose>
        <c:when test="${not sessionScope.user.admin}">
            <a href="${pageContext.servletContext.contextPath}/controller?command=showPayments">
                <div class="main-container-child">
                    <i class="fas fa-hand-holding-usd fa-3x"></i>
                    <br>
                    <span><fmt:message key="label.payments"/></span>
                </div>
            </a>
            <a href="${pageContext.servletContext.contextPath}/controller?command=showTariffs">
                <div class="main-container-child">
                    <i class="fas fa-signal fa-3x"></i>
                    <br>
                    <span><fmt:message key="title.tariffs"/></span>
                </div>
            </a>
            <a href="${pageContext.servletContext.contextPath}/controller?command=showTraffic">
                <div class="main-container-child">
                    <i class="fas fa-wifi fa-3x"></i>
                    <br>
                    <span><fmt:message key="label.traffic"/></span>
                </div>
            </a>
        </c:when>
        <c:otherwise>
            <a href="${pageContext.servletContext.contextPath}/controller?command=showUsers">
                <div class="main-container-child">
                    <i class="fas fa-user-alt fa-3x"></i>
                    <br>
                    <span><fmt:message key="title.users"/></span>
                </div>
            </a>
            <a href="${pageContext.servletContext.contextPath}/controller?command=showTariffsAdmin">
                <div class="main-container-child">
                    <i class="fas fa-signal fa-3x"></i>
                    <br>
                    <span><fmt:message key="title.tariffs"/></span>
                </div>
            </a>
            <a href="${pageContext.servletContext.contextPath}/controller?command=showDiscounts">
                <div class="main-container-child">
                    <i class="fas fa-percentage fa-3x"></i>
                    <br>
                    <span><fmt:message key="title.discounts"/></span>
                </div>
            </a>
        </c:otherwise>
    </с:choose>
    <a href="${pageContext.servletContext.contextPath}/controller?command=showSettings">
        <div class="main-container-child">
            <i class="fas fa-cog fa-3x"></i>
            <br>
            <span><fmt:message key="title.settings"/></span>
        </div>
    </a>
</div>
<с:if test="${not sessionScope.user.admin and sessionScope.user.idTariff eq 0}">
    <script>
        showMessage("<fmt:message key='message.deletedTariff'/>", "${pageContext.servletContext.contextPath}/controller?command=showTariffs");
    </script>
</с:if>
</body>
</html>
