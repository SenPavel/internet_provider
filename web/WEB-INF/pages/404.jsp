<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title>404</title>
    <link rel="stylesheet" type="text/css" href="../../styles/404.css"/>
</head>
<body>
<div class="bg-image">
    <a href="${pageContext.servletContext.contextPath}/controller?command=showMain">
        <h1><fmt:message key="message.pageNotFound"/></h1>
    </a>
</div>
</body>
</html>
