<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="title.tariffs"/> </title>
    <link rel="stylesheet" type="text/css" href="../../styles/table.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/toastr.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/tariffs.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/tariffs-media.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/table-media.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/snackbar.css"/>
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery-3.3.1.min.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/snackbar.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastr.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastrMessages.js"/>'></script>
</head>

<body>
<jsp:include page="/WEB-INF/fragments/menu.jsp"/>
<div class="bg-image-tariffs"></div>
<div class="tariff-container">
    <div class="table-container">
        <div class="table-container-child">
            <table>
                <c:choose>
                    <c:when test="${sessionScope.user.idTariff ne 0}">
                        <c:forEach items="${requestScope.tariffs}" var="tariff">
                            <c:if test="${sessionScope.user.idTariff eq tariff.id}">
                                <caption><fmt:message key="caption.currentTariff"/>: ${tariff.name}</caption>
                            </c:if>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <caption><fmt:message key="message.notSelectTariff"/> </caption>
                    </c:otherwise>
                </c:choose>
                <thead>
                <tr>
                    <th scope="col"><fmt:message key="label.name"/></th>
                    <th scope="col"><fmt:message key="label.downloadSpeed"/></th>
                    <th scope="col"><fmt:message key="label.unloadSpeed"/></th>
                    <th scope="col"><fmt:message key="label.price"/></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.tariffs}" var="tariff">
                    <tr>
                        <td data-label="<fmt:message key="label.name"/>">${tariff.name}</td>
                        <td data-label="<fmt:message key="label.downloadSpeed"/>">${tariff.downloadSpeed}</td>
                        <td data-label="<fmt:message key="label.unloadSpeed"/>">${tariff.unloadSpeed}</td>
                        <td data-label="<fmt:message key="label.price"/>">${tariff.discountPrice}</td>
                        <td data-label="" height="40px">
                            <c:if test="${sessionScope.user.idTariff ne tariff.id}">
                                <form method="post" action="${pageContext.servletContext.contextPath}/controller?command=changeUserTariff&id_tariff=${tariff.id}&id_user=${sessionScope.user.id}">
                                    <button type="submit" onclick="return confirm('<fmt:message key="message.tariffConfirm"/> \'${tariff.name}\'')">
                                        <fmt:message key="label.order"/>
                                    </button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<c:if test="${sessionScope.successful eq true}">
    <script type="text/javascript">
        successToastr("<fmt:message key='message.successfullyChangeTariff'/>");
    </script>
    <c:remove var="successful" scope="session"/>
</c:if>
</body>
</html>
