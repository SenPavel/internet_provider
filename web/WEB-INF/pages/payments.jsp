<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="m" uri="http://www.tagLib.com" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="label.payments"/> </title>
    <link rel="stylesheet" type="text/css" href="../../styles/table.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/table-media.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/payments.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/payments-media.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/snackbar.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/toastr.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/pagination.css"/>
    <link rel="stylesheet" type="text/css" href="../../styles/media/pagination-media.css"/>
    <script type="text/javascript" src='<c:url value="../../scripts/lib/jquery-3.3.1.min.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastrMessages.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/snackbar.js"/>'></script>
    <script type="text/javascript" src='<c:url value="../../scripts/toastr.js"/>'></script>
</head>
<header>
</header>
<body>
<jsp:include page="/WEB-INF/fragments/menu.jsp"/>
<div class="bg-image-tariffs"></div>
<div class="payment-container">
    <div class="payment-form">
        <form action="${pageContext.servletContext.contextPath}/controller?command=addPayment" method="post">
            <label><fmt:message key="label.topUpBalance"/>
                <input type="text" id="pay_in" name="pay_in" required="" pattern="^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$"
                       oninvalid="this.setCustomValidity('<fmt:message key='error.incorrectPayment'/>')"
                       oninput="this.setCustomValidity('');">
            </label>
            <input type="submit" value="<fmt:message key="label.topUp"/> ">
        </form>
    </div>
    <div class="table-container">
        <div class="table-container-child">
            <table>
                <thead>
                <tr>
                    <th scope="col"><fmt:message key="label.operation"/></th>
                    <th scope="col"><fmt:message key="label.amount"/></th>
                    <th scope="col"><fmt:message key="label.date"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.payments}" var="payment">
                    <tr>
                        <c:choose>
                            <c:when test="${payment.payIn gt 0}">
                                <td data-label="<fmt:message key="label.operation"/>"><fmt:message key="label.payIn"/> </td>
                                <td data-label="<fmt:message key="label.amount"/>">${payment.payIn}</td>
                            </c:when>
                            <c:otherwise>
                                <td data-label="<fmt:message key="label.operation"/>"><fmt:message key="label.payOut"/></td>
                                <td data-label="<fmt:message key="label.amount"/>">${payment.payOut}</td>
                            </c:otherwise>
                        </c:choose>
                        <td data-label="<fmt:message key="label.date"/>">
                            <fmt:formatDate type="date" value="${payment.date}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <m:pagination link="${pageContext.servletContext.contextPath}/controller?command=showPayments" currentPage="${requestScope.currentPage}" pagesCount="${requestScope.pagesCount}"/>
        </div>
    </div>
</div>
<c:if test="${sessionScope.successful eq true}">
    <script type="text/javascript">
        successToastr("<fmt:message key='message.successfullyTopUp'/>");
    </script>
    <c:remove var="successful" scope="session"/>
</c:if>
<c:if test="${not empty sessionScope.errorMessage}">
    <script type="text/javascript">
        successToastr("<fmt:message key='${sessionScope.errorMessage}'/>");
    </script>
    <c:remove var="errorMessage" scope="session"/>
</c:if>
</body>
<script src="../../scripts/pagination.js" type="text/javascript"></script>
</html>
